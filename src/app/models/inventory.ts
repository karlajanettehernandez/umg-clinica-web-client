export interface MedicineInventoryModel {
  id?: number;
  medicineId: number;
  medicineName?: string;
  purchasePrice: number;
  salePrice: number;
  batchNumber: string;
  quantity: number;
  expirationDate: Date;
  quantitySale?: number;
  stock?: number;
  active?: boolean;
  comments?: string;
}
