export class MedicineModel {
  id?: number;
  name?: string;
}

export class LabTestModel {
  id?: number;
  name?: string;
}
