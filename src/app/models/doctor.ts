export interface DoctorModel {
  id: number;
  name: string;
  lastName: string;
  email: string;
  birthDate: string;
  genderId: number;
  genderName: string;
  phoneNumber: string;
  address: string;
  activeCollegiate: string;
  identificationDocumentNumber: string;
  identificationDocumentTypeId: number;
  identificationDocumentTypeName: string;
  specialtyId: number;
  specialtyName: string;
  active: boolean;
  createdAt: string;
  createdBy: string;
  modifiedAt: string;
  modifiedBy: string;
}

export interface DoctorSpecialtyModel {
  id: number;
  name: string;
}
