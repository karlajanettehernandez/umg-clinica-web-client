export interface GenericDialogData {
  id?: number;
  operationType?: string;
  payload?: any;
}

export interface PersonGenderModel {
  id?: number;
  active?: boolean;
  name: string;
}

export interface SpecialtyModel {
  id?: number;
  active?: boolean;
  name: string;
}

export interface DocumentTypeModel {
  id?: number;
  active?: boolean;
  name: string;
}
