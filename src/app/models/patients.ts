export interface PatientModel {
  id: number;
  name: string;
  lastName: string;
  email: string;
  birthDate: string;
  genderId: number;
  phoneNumber: string;
  address: string;
  identificationDocumentNumber: string;
  identificationDocumentTypeId: number;
  active: boolean;
  schedule?: number;
}
