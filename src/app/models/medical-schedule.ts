export interface MedicalScheduleModel {
  id?: number;
  medicalScheduleStatusId?: number;
  startAt: string;
  endAt: string;
  note: string;
  patientId: number;
  patientName: string;
  doctorId: number;
  doctorName?: string;
  createdBy?: string;
}
