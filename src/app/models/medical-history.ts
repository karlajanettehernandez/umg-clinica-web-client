export interface MedicalHistoryModel {
  id?: number;
  doctorId?: number;
  doctorName?: string;
  patientId?: number;
  patientName?: string;
  description?: string;
  diagnostic?: string;
  labTestList?: MedicalHistoryLabTestModel[];
  prescriptionList?: MedicalHistoryPrescriptionModel[];
  inventoryReferenceList?: MedicalHistoryInventoryReferenceModel[];
  createdAt?: Date | string;
  createdBy?: string;
  modifiedAt?: string;
  modifiedBy?: string;
}

export interface MedicalHistoryLabTestModel {
  id?: number;
  labTestId: number;
  labTestName: string;
  description: string;
}

export interface MedicalHistoryPrescriptionModel {
  id?: number;
  medicineId: number;
  medicineName: string;
  indications: string;
}

export interface MedicalHistoryInventoryReferenceModel {
  id?: number;
  medicineInventoryId?: number;
  medicineName?: string;
  quantity?: number;
}
