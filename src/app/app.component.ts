import {Component} from '@angular/core';
import {AuthenticationHttpService} from './http-services/user/authentication.http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private authenticationService: AuthenticationHttpService) {
    this.authenticationService.verifyToken();
  }

}
