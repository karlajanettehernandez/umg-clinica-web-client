import {Component, ContentChild, Directive, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CdkPortal} from '@angular/cdk/portal';

@Directive({
  selector: '[appCardLayoutContent]',
})
export class CardLayoutContentDirective extends CdkPortal {
}

@Component({
  selector: 'app-card-layout',
  templateUrl: './card-layout.component.html',
  styleUrls: ['./card-layout.component.scss']
})
export class CardLayoutComponent implements OnInit {

  /**
   * template para establecer el contenido customizado de la pagina
   */
  @ContentChild(CardLayoutContentDirective, {static: true})
  templateContent: CardLayoutContentDirective;

  @Input()
  floatActionButtonTranslateKey = '';

  @Input()
  showFloatActionButton = true;

  @Input()
  floatActionButtonDisabled = false;

  @Input()
  floatActionButtonIcon = 'add';

  /**
   * Establece la accion que va a realizar el boton flotante
   */
  @Output()
  readonly floatActionButtonOutput: EventEmitter<void> = new EventEmitter<void>();

  constructor() {
  }

  onActionButton(): void {
    this.floatActionButtonOutput.emit();
  }

  ngOnInit(): void {
  }

}
