import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

  searchInput = '';

  @Output()
  searchOutput = new EventEmitter<string>();

  constructor() {
  }

  search(): void {
    this.searchOutput.next(this.searchInput);
  }

  ngOnInit(): void {
  }

}
