import { Component, Input, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AuthenticationHttpService } from '../../http-services/user/authentication.http.service';
import { Router } from '@angular/router';
import { ToolbarService } from './toolbar.service';

const DEFAULT_PHOTO_URL = 'assets/images/avatars/profile.jpg';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit, OnDestroy {

  private unsubscribeAll = new Subject<boolean>();

  userName = 'Admin';
  userEmail = 'admin@email.com';
  photoURL = DEFAULT_PHOTO_URL;
  searchInput = '';

  @Input()
  pageTitle = '';

  @Input()
  showSearchBar = false;
  loading = false;

  @Output()
  toggleMenuEventOutput = new EventEmitter<void>();

  constructor(
    private readonly router: Router,
    private readonly authenticationService: AuthenticationHttpService,
    private readonly toolbarService: ToolbarService
  ) {
  }

  logout(): void {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

  buscar(): void {
    this.toolbarService.search(this.searchInput ?? '');
  }

  onToggleMenu(): void {
    this.toggleMenuEventOutput.emit();
  }

  ngOnInit(): void {
    this.authenticationService
      .userInfo
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(userInfo => {
        if (userInfo) {
          setTimeout(() => {

            this.userName = userInfo.name;
            this.userEmail = userInfo.sub;

          }, 10);
        }
      });

    this.toolbarService
      .pageTitle
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(value => {

        setTimeout(() => {

          this.pageTitle = value;

        }, 10);

      });

    this.toolbarService
      .showSearchBar
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(value => {
        setTimeout(() => {
          this.showSearchBar = value;
        }, 10);
      });

    this.toolbarService
      .isLoading
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(value => {
        setTimeout(() => {
          this.loading = value;
        }, 10);
      });
  }

  ngOnDestroy(): void {
    this.unsubscribeAll.next(true);
    this.unsubscribeAll.complete();
  }

}
