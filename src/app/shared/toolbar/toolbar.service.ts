import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { RoleModel } from '../../models/user';

@Injectable()
export class ToolbarService {

  private readonly pageTitleSubject = new Subject<string>();
  private readonly searchTextSubject = new Subject<string>();
  private readonly showSearchBarSubject = new Subject<boolean>();
  private readonly isLoadingSubject = new Subject<boolean>();
  private readonly rolesSubject = new BehaviorSubject<RoleModel[]>([]);

  readonly pageTitle = this.pageTitleSubject.asObservable();
  readonly showSearchBar = this.showSearchBarSubject.asObservable();
  readonly isLoading = this.isLoadingSubject.asObservable();
  readonly roles = this.rolesSubject.asObservable();

  private permissions: string[] = [];

  setPermissions(permissions: string[]): void {
    this.permissions = permissions;
  }

  hasPermission(permission: string): boolean {
    return this.permissions.indexOf(permission) > -1;
  }

  search(text: string): void {
    this.searchTextSubject.next(text);
  }

  setPageTitle(title: string): void {
    this.pageTitleSubject.next(title);
  }

  setShowSearchBar(value: boolean): void {
    this.showSearchBarSubject.next(value);
  }

  showProgressBar(value: boolean): void {
    this.isLoadingSubject.next(value);
  }

}
