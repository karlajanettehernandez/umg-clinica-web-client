import {NgModule} from '@angular/core';
import {MainLayoutComponent, MainLayoutContentDirective, MainLayoutToolbarDirective} from './main-layout.component';
import {CommonModule} from '@angular/common';
import {PortalModule} from '@angular/cdk/portal';
import {FlexLayoutModule} from '@angular/flex-layout';

@NgModule({
  declarations: [
    MainLayoutComponent,
    MainLayoutToolbarDirective,
    MainLayoutContentDirective
  ],
  exports: [
    MainLayoutComponent,
    MainLayoutToolbarDirective,
    MainLayoutContentDirective
  ],
  imports: [
    CommonModule,
    PortalModule,
    FlexLayoutModule
  ]
})
export class MainLayoutModule {
}
