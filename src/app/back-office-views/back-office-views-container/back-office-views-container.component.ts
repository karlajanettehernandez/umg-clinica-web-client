import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { ApplicationMenuHttpService } from '../../http-services/application/application-menu.http.service';
import { ToolbarService } from '../../shared/toolbar/toolbar.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ApplicationMenu } from '../../models/application';
import { buildMenuTree } from '../../utils/shared-functions';
import { AuthenticationHttpService } from '../../http-services/user/authentication.http.service';
import { RoleHttpService } from '../../http-services/user/role.http.service';
import { RoleModel } from '../../models/user';

@Component({
  selector: 'app-back-office-views-container',
  templateUrl: './back-office-views-container.component.html',
  styleUrls: ['./back-office-views-container.component.scss']
})
export class BackOfficeViewsContainerComponent implements OnInit {

  private readonly unsubscribeAll = new Subject<boolean>();

  @ViewChild('backOfficeMatDrawer', { static: false })
  mainMatDrawer: MatDrawer;

  parentMenus: ApplicationMenu[] = [];
  roles: RoleModel[] = [];

  loading = true;

  constructor(
    private router: Router,
    private readonly applicationMenuHttpService: ApplicationMenuHttpService,
    private readonly toolbarService: ToolbarService,
    private readonly authenticationService: AuthenticationHttpService,
    private readonly roleHttpService: RoleHttpService
  ) {
  }

  toggleMenu(): void {
    if (this.mainMatDrawer) {
      this.mainMatDrawer.toggle();
    }
  }

  goToMenu(path: string): void {
    this.router.navigate([path]);
  }

  loadUserRoles(userId: number): void {
    this.roleHttpService
      .getRolesByUserId(userId)
      .then(roles => {
        this.roles = [...roles];

        if (roles.length > 0) {
          this.loadMenus(roles[0].id);
        }
      })
      .catch(err => console.error(err));
  }

  private loadMenus(roleId: number): void {
    this.toolbarService.showProgressBar(true);
    this.applicationMenuHttpService
      .getMenusByRoleId(roleId)
      .then(menus => {
        let permissions: string[] = [];

        menus.forEach(it => {
          if (it.permissions && it.permissions.length > 0) {
            permissions = [...permissions, ...it.permissions.map(permission => permission.code)];
          }
        });

        this.toolbarService.setPermissions(permissions);

        this.parentMenus = buildMenuTree(menus);
        this.selectDefaultMenu();
      })
      .catch(err => console.error(err))
      .finally(() => {
        this.loading = false;
        this.toolbarService.showProgressBar(false);
      });
  }

  private selectDefaultMenu(): void {
    if (this.parentMenus.length == 0) {
      return;
    }

    if (this.router.url === '/back-office' || this.router.url === '/back-office/') {
      const firstParentMenu = this.parentMenus[0];

      if (firstParentMenu.children.length > 0) {
        const childMenu = firstParentMenu.children[0];
        this.goToMenu(childMenu.path);
      }
    }
  }

  ngOnInit(): void {
    this.authenticationService
      .userInfo
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(userInfo => {
        if (userInfo) {
          this.loadUserRoles(userInfo.userId);
        }
      });
  }

}
