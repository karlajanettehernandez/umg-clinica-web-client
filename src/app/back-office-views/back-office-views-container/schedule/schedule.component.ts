import { Component, OnInit } from '@angular/core';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarMonthViewDay
} from 'angular-calendar';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { GenericDialogData } from '../../../models/common';
import { EventFormDialogComponent } from './event-form-dialog/event-form-dialog.component';
import { MatSnackBarViewService } from '../../../views-services/mat-snack-bar.view.service';
import { MedicalScheduleHttpService } from '../../../http-services/medical-schedule/medical-schedule.http.service';
import { isSameDay, isSameMonth, startOfDay } from 'date-fns';
import { MedicalScheduleModel } from '../../../models/medical-schedule';
import { ToolbarService } from '../../../shared/toolbar/toolbar.service';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import {
  ConfirmationDialogComponent,
  ConfirmationDialogData
} from '../../../shared/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss'],
})
export class ScheduleComponent implements OnInit {

  private readonly datePipe = new DatePipe('en-US');

  medicalScheduleList: MedicalScheduleModel[] = [];
  events: CalendarEvent[] = [];
  actions: CalendarEventAction[] = [];
  activeDayIsOpen = false;
  refresh: Subject<boolean> = new Subject();
  selectedDay = { date: startOfDay(new Date()) };
  view: 'month' | 'week' | 'day' = 'month';
  viewDate = new Date();

  constructor(
    private readonly matDialog: MatDialog,
    private readonly matSnackBarViewService: MatSnackBarViewService,
    private readonly medicalScheduleHttpService: MedicalScheduleHttpService,
    private readonly toolbarService: ToolbarService,
    private readonly router: Router,
  ) {
    this.toolbarService.setShowSearchBar(false);
    this.actions = [
      {
        label: '<i class="material-icons s-16">edit</i>',
        onClick: ({ event }: { event: CalendarEvent }): void => {
          this.editEvent('edit', event);
        }
      },
      {
        label: '<i class="material-icons s-16">delete</i>',
        onClick: ({ event }: { event: CalendarEvent }): void => {
          this.deleteEvent(event);
        }
      }
    ];
  }

  setEvents(): void {
  }

  beforeMonthViewRender({ header, body }): void {
    const selectedDay = body.find(day => {
      return day.date.getTime() === this.selectedDay.date.getTime();
    });

    if (selectedDay) {
      selectedDay.cssClass = 'cal-selected';
    }
  }

  dayClicked(day: CalendarMonthViewDay): void {
    const date: Date = day.date;
    const events: CalendarEvent[] = day.events;

    if (isSameMonth(date, this.viewDate)) {
      if ((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
    this.selectedDay = day;
    this.refresh.next(true);
  }

  eventTimesChanged({ event, newStart, newEnd }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.refresh.next(true);
  }

  deleteEvent(event): void {
    const id = event.id as number;
    const data = this.medicalScheduleList.find(it => it.id === id);

    const dialogRef = this.matDialog
      .open<ConfirmationDialogComponent, ConfirmationDialogData, boolean>(ConfirmationDialogComponent, {
        width: '400px',
        data: {
          title: 'Eliminar cita',
          question: `Confirma que quiere eliminar la cita de ${data.patientName}?`
        }
      });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.toolbarService.showProgressBar(true);

        this.medicalScheduleHttpService
          .delete(event.id)
          .then(() => {
            this.matSnackBarViewService.showSnackBar('Cita eliminada exitosamente!', 'OK');
            this.loadEvents();
          })
          .catch(err => {
            console.error(err);
            this.matSnackBarViewService.showSnackBar('No es posible realizar esta acción, intente mas tarde', 'OK', false);
          })
          .finally(() => this.toolbarService.showProgressBar(false))
      }
    });
  }

  goToMedicalHistory(action: string, event: CalendarEvent): void {
    const id = event.id as number;
    const data = this.medicalScheduleList.find(it => it.id === id);

    const queryParams = {
      schedule: id,
      doctorId: data.doctorId,
      patientName: data.patientName
    };

    this.router.navigate(['back-office', 'patients', `${data.patientId}`, 'medical-history'], { queryParams });
  }

  editEvent(action: string, event: CalendarEvent): void {
    const id = event.id as number;
    const data = this.medicalScheduleList.find(it => it.id === id);

    if (data) {
      const dialogRef = this.matDialog
        .open<EventFormDialogComponent, GenericDialogData, boolean>(EventFormDialogComponent, {
          width: '500px',
          height: '75%',
          panelClass: 'mat-dialog-without-padding',
          data: {
            operationType: 'EDIT',
            payload: data
          }
        });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.matSnackBarViewService.showSnackBar('Cita actualizada exitosamente!');
          this.loadEvents();
        }
      });
    }
  }

  addEvent(): void {
    const dialogRef = this.matDialog
      .open<EventFormDialogComponent, GenericDialogData, boolean>(EventFormDialogComponent, {
        width: '500px',
        height: '75%',
        panelClass: 'mat-dialog-without-padding',
        data: {
          operationType: 'ADD',
        }
      });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.matSnackBarViewService.showSnackBar('Cita agendada exitosamente!');
        this.loadEvents();
      }
    });
  }

  private loadEvents(): void {
    this.toolbarService.showProgressBar(true);
    this.medicalScheduleHttpService
      .getAll()
      .then(data => {
        this.events = [];
        this.medicalScheduleList = [...data];

        data.forEach(it => {
          const startDate = new Date(it.startAt);
          const endDate = new Date(it.endAt);
          const startHour = this.datePipe.transform(startDate, 'HH:mm');
          const endHour = this.datePipe.transform(endDate, 'HH:mm');
          const createdBy = it.createdBy;
          const eventTitle = `${startHour} a ${endHour} - Paciente ${it.patientName}, ${it.doctorName}, agendado por ${createdBy}`;

          this.events.push({
            id: it.id,
            start: startDate,
            end: endDate,
            title: eventTitle,
            color: { primary: '#ff4081', secondary: '#3f51b5' },
            allDay: false,
            actions: [...this.actions]
          });
        });

        this.refresh.next(true);
      })
      .catch(err => console.error(err))
      .finally(() => this.toolbarService.showProgressBar(false));
  }

  ngOnInit(): void {
    this.loadEvents();
  }

}
