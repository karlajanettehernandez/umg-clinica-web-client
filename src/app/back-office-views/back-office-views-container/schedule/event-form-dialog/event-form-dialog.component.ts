import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { GenericDialogData } from '../../../../models/common';
import { MedicalScheduleModel } from '../../../../models/medical-schedule';
import { DatePipe } from '@angular/common';
import { MedicalScheduleHttpService } from '../../../../http-services/medical-schedule/medical-schedule.http.service';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBarViewService } from '../../../../views-services/mat-snack-bar.view.service';
import { DoctorHttpService } from '../../../../http-services/doctor/doctor.http.service';
import { DoctorModel } from '../../../../models/doctor';
import { searchByLowerCaseText } from '../../../../constants/constants';
import { ReplaySubject, Subject } from 'rxjs';
import { PatientHttpService } from '../../../../http-services/patient/patient.http.service';
import { PatientModel } from '../../../../models/patients';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-event-form-dialog',
  templateUrl: './event-form-dialog.component.html',
  styleUrls: ['./event-form-dialog.component.scss']
})
export class EventFormDialogComponent implements OnInit, OnDestroy {

  private readonly unsubscribeAll = new Subject<boolean>();
  private readonly datePipe = new DatePipe('en-US');

  loading = false;
  isEditMode = false;
  eventForm: FormGroup;
  dialogTitle = 'Agendar cita';
  okButton = 'Crear cita';
  allPatients: PatientModel[] = [];
  filteredPatients = new ReplaySubject<PatientModel[]>(1);
  allDoctors: DoctorModel[] = [];
  filteredDoctors = new ReplaySubject<DoctorModel[]>(1);

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<GenericDialogData, boolean>,
    @Inject(MAT_DIALOG_DATA) public data: GenericDialogData,
    private readonly medicalScheduleHttpService: MedicalScheduleHttpService,
    private readonly matSnackBarViewService: MatSnackBarViewService,
    private readonly doctorHttpService: DoctorHttpService,
    private readonly patientHttpService: PatientHttpService,
  ) {
    if (data.operationType === 'EDIT') {
      this.isEditMode = true;
      this.dialogTitle = 'Editar cita';
      this.okButton = 'Guardar cambios';

      const event = data.payload as MedicalScheduleModel;

      this.eventForm = this.formBuilder.group({
        id: [event.id, Validators.required],
        medicalScheduleStatusId: [event.medicalScheduleStatusId],
        doctorId: [event.doctorId, Validators.required],
        isExistingPatient: [event.patientId !== null && event.patientId !== undefined && event.patientId !== 0],
        patientId: [event.patientId],
        patientName: [event.patientName, Validators.required],
        note: [event.note, Validators.required],
        eventDate: [new Date(event.startAt), Validators.required],
        startTime: [this.getTimeFromDate(new Date(event.startAt)), Validators.required],
        endTime: [this.getTimeFromDate(new Date(event.endAt)), Validators.required],
        active: [true]
      });

    } else {

      this.eventForm = this.formBuilder.group({
        id: [0, Validators.required],
        medicalScheduleStatusId: [0],
        doctorId: [null, Validators.required],
        isExistingPatient: [false],
        patientId: [null],
        patientName: [null, Validators.required],
        note: ['', Validators.required],
        eventDate: [new Date(), Validators.required],
        startTime: [this.getTimeFromDate(new Date()), Validators.required],
        endTime: [this.getTimeFromDate(new Date(), 30), Validators.required],
        active: [true],
      });
    }
  }

  private getTimeFromDate(date: Date, addMinutes?: number): string {
    if (addMinutes) {
      date.setMinutes(date.getMinutes() + addMinutes);
    }

    return this.datePipe.transform(date, 'HH:mm');
  }

  cancelar(): void {
    this.dialogRef.close(false);
  }

  aceptar(): void {
    if (this.eventForm.invalid) {
      return;
    }

    this.loading = true;

    const eventDate = this.datePipe.transform(this.eventForm.controls.eventDate.value, 'yyyy-MM-dd');

    const data: MedicalScheduleModel = {
      startAt: `${eventDate}T${this.eventForm.controls.startTime.value}:00`,
      endAt: `${eventDate}T${this.eventForm.controls.endTime.value}:00`,
      note: this.eventForm.controls.note.value,
      patientId: this.eventForm.controls.patientId.value,
      patientName: this.eventForm.controls.patientName.value,
      doctorId: this.eventForm.controls.doctorId.value
    };

    if (this.isEditMode) {
      data.id = this.eventForm.controls.id.value;
      data.medicalScheduleStatusId = this.eventForm.controls.medicalScheduleStatusId.value;

      this.medicalScheduleHttpService
        .update(data)
        .then(() => this.dialogRef.close(true))
        .catch(err => this.processError(err))
        .finally(() => this.loading = false);
    } else {
      this.medicalScheduleHttpService
        .create(data)
        .then(response => {
          if (response > 0) {
            this.dialogRef.close(true);
          }
        })
        .catch(err => this.processError(err))
        .finally(() => this.loading = false);
    }
  }

  onFilterDoctors(filterValue: string): void {
    if (filterValue) {
      const result = this.allDoctors.filter(doctor => searchByLowerCaseText(doctor.name, filterValue));
      this.filteredDoctors.next(result);
    } else {
      this.filteredDoctors.next([...this.allDoctors]);
    }
  }

  onFilterPatients(filterValue: string): void {
    if (filterValue) {
      const result = this.allPatients.filter(patient => this.searchPatient(patient, filterValue));
      this.filteredPatients.next(result);
    } else {
      this.filteredPatients.next([...this.allPatients]);
    }
  }

  onChangeIsExistingPatient(): void {
    if (!this.eventForm.controls.isExistingPatient.value) {
      this.eventForm.controls.patientId.setValue(null);
    }
  }

  onSelectPatient(): void {
    const id = this.eventForm.controls.patientId.value;
    if (id) {
      const patient = this.allPatients.find(it => it.id === id);

      if (patient) {
        this.eventForm.controls.patientName.setValue(`${patient.name} ${patient.lastName}`);
      }
    }
  }

  private searchPatient(patient: PatientModel, filterValue: string): boolean {
    return searchByLowerCaseText(patient.name, filterValue)
      || searchByLowerCaseText(patient.identificationDocumentNumber, filterValue);
  }

  private processError(err: any): void {
    const errorResponse = err as HttpErrorResponse;

    if (errorResponse.error.mensaje) {
      this.matSnackBarViewService.showSnackBar(errorResponse.error.mensaje, 'OK', false);
    }
  }

  private loadDoctors(): void {
    this.doctorHttpService
      .getList()
      .then(doctors => {
        this.allDoctors = [...doctors];
        this.filteredDoctors.next([...doctors])
      })
      .catch(err => console.error(err));
  }

  private loadPatients(): void {
    this.patientHttpService
      .getAll()
      .then(patients => {
        this.allPatients = [...patients];
        this.filteredPatients.next([...patients]);
      })
      .catch(err => console.error(err));
  }

  ngOnInit(): void {
    this.loadDoctors();
    this.loadPatients();

    this.eventForm
      .controls
      .startTime
      .valueChanges
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(value => {
        if (value) {
          const parts = value.split(':');
          if (parts.length == 2) {
            const startDate = new Date();
            startDate.setHours(parseInt(parts[0]), parseInt(parts[1]))

            const endDate = new Date();
            endDate.setHours(startDate.getHours());
            endDate.setMinutes(startDate.getMinutes() + 30);

            this.eventForm.controls.endTime.setValue(this.datePipe.transform(endDate, 'HH:mm'));
          }
        }
      })
  }

  ngOnDestroy() {
    this.unsubscribeAll.next(true);
    this.unsubscribeAll.complete();
  }

}
