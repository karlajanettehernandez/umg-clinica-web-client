import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { RouterModule, Routes } from '@angular/router';
import { DashboardCardModule } from '../../../shared/dashboard-card/dashboard-card.module';
import { FloatingSpinnerModule } from '../../../shared/floating-spinner/floating-spinner.module';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent
  }
];

@NgModule({
  declarations: [
    DashboardComponent
  ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        DashboardCardModule,
        FloatingSpinnerModule
    ]
})
export class DashboardModule {
}
