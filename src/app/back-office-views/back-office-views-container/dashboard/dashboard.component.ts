import { Component, OnInit } from '@angular/core';
import { DashboardHttpService } from '../../../http-services/clinic/dashboard.http.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  loadingPatientsAttendedToday = true;
  loadingPatientsAttendedInTheMonth = true;
  loadingPatientsAttendedInTheLastMonth = true;

  quantityPatientsAttendedToday = 0;
  quantityPatientsAttendedInTheMonth = 0;
  quantityPatientsAttendedInTheLastMonth = 0;

  constructor(private readonly dashboardHttpService: DashboardHttpService) {
  }

  ngOnInit(): void {
    this.dashboardHttpService
      .patientsAttendedToday()
      .then(quantity => this.quantityPatientsAttendedToday = quantity)
      .catch(err => console.error(err))
      .finally(() => this.loadingPatientsAttendedToday = false);

    this.dashboardHttpService
      .patientsAttendedInTheMonth()
      .then(quantity => this.quantityPatientsAttendedInTheMonth = quantity)
      .catch(err => console.error(err))
      .finally(() => this.loadingPatientsAttendedInTheMonth = false);

    this.dashboardHttpService
      .patientsAttendedInTheLastMonth()
      .then(quantity => this.quantityPatientsAttendedInTheLastMonth = quantity)
      .catch(err => console.error(err))
      .finally(() => this.loadingPatientsAttendedInTheLastMonth = false);
  }

}
