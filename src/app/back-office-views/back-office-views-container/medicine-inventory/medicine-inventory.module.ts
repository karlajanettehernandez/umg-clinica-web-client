import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MedicineInventoryComponent} from './medicine-inventory.component';
import {RouterModule, Routes} from "@angular/router";
import {CardLayoutModule} from "../../../shared/card-layout/card-layout.module";
import {AgGridModule} from "ag-grid-angular";
import { FlexLayoutModule } from '@angular/flex-layout';
import { SearchBarModule } from '../../../shared/search-bar/search-bar.module';
import { MatButtonModule } from '@angular/material/button';
import { MedicineInventoryDialogComponent } from './medicine-inventory-dialog/medicine-inventory-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MAT_DATE_LOCALE, MatNativeDateModule } from '@angular/material/core';
import { MatSelectSearchModule } from '../../../shared/mat-select-search/mat-select-search.module';

const routes: Routes = [
  {
    path: '',
    component: MedicineInventoryComponent
  }
];

@NgModule({
  declarations: [
    MedicineInventoryComponent,
    MedicineInventoryDialogComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CardLayoutModule,
    AgGridModule,
    FlexLayoutModule,
    SearchBarModule,
    MatButtonModule,
    MatDialogModule,
    MatProgressBarModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectSearchModule,
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }
  ]
})
export class MedicineInventoryModule {
}
