import { Component, OnInit } from '@angular/core';
import { MedicineInventoryModel } from "../../../models/inventory";
import {
  AgGridIconButtonComponent
} from "../../../shared/ag-grid-components/ag-grid-icon-button/ag-grid-icon-button.component";
import {
  AgGridIconCheckComponent
} from "../../../shared/ag-grid-components/ag-grid-icon-check/ag-grid-icon-check.component";
import { ColDef, GridOptions } from "ag-grid-community";
import { MedicineInventoryHttpService } from '../../../http-services/clinic/medicine-inventory.http.service';
import { ToolbarService } from '../../../shared/toolbar/toolbar.service';
import { GenericDialogData } from '../../../models/common';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBarViewService } from '../../../views-services/mat-snack-bar.view.service';
import { MedicineInventoryDialogComponent } from './medicine-inventory-dialog/medicine-inventory-dialog.component';
import { DATE_DD_MM_YYYY_FORMAT, searchByLowerCaseText } from '../../../constants/constants';
import { DatePipe } from '@angular/common';
import {
  ConfirmationDialogComponent,
  ConfirmationDialogData
} from '../../../shared/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-medicine-inventory',
  templateUrl: './medicine-inventory.component.html',
  styleUrls: ['./medicine-inventory.component.scss']
})
export class MedicineInventoryComponent implements OnInit {

  private readonly datePipe = new DatePipe('en-US');

  allInventoryList: MedicineInventoryModel[] = [];
  inventoryList: MedicineInventoryModel[] = [];

  gridOptions: GridOptions = {
    rowHeight: 30
  };

  defaultColDef = {
    resizable: true
  };

  columnDefs: ColDef[] = [
    {
      headerName: 'ID',
      field: 'id',
      width: 50
    },
    {
      headerName: 'Nombre',
      field: 'medicineName',
      width: 150
    },
    {
      headerName: 'Cantidad',
      field: 'quantity',
      width: 150
    },
    {
      headerName: 'Cantidad vendida',
      field: 'quantitySale',
      width: 150
    },
    {
      headerName: 'Existencia',
      field: 'stock',
      width: 150
    },
    {
      headerName: 'Precio de compra',
      field: 'purchasePrice',
      width: 150
    },
    {
      headerName: 'Precio de venta',
      field: 'salePrice',
      width: 150
    },
    {
      headerName: 'Fecha de vencimiento',
      field: 'expirationDate',
      width: 160,
      cellRenderer: params => this.datePipe.transform(params.value, DATE_DD_MM_YYYY_FORMAT)
    },
    {
      headerName: 'Número de lote',
      field: 'batchNumber',
      width: 150
    },
    {
      headerName: 'Activo',
      field: 'active',
      cellRendererFramework: AgGridIconCheckComponent,
      width: 90
    },
    {
      headerName: 'Comentarios',
      field: 'comments',
      width: 300
    },
    {
      headerName: 'Editar',
      field: 'edit',
      width: 90,
      cellRendererFramework: AgGridIconButtonComponent,
      cellRendererParams: {
        iconName: 'edit',
        buttonTitle: 'Editar',
        onAction: (data: MedicineInventoryModel) => this.edit(data)
      },
      pinned: 'right'
    },
    {
      headerName: 'Desactivar',
      field: 'deleteUser',
      width: 125,
      cellRendererFramework: AgGridIconButtonComponent,
      cellRendererParams: {
        iconName: 'cancel',
        buttonTitle: 'Desactivar inventario',
        onAction: (data: MedicineInventoryModel) => this.delete(data),
        onShowButton: (data: MedicineInventoryModel) => {
          const expirationDate = new Date(`${data.expirationDate}T23:59:59-06:00`);
          const currentDate = new Date();
          return data.active && currentDate.getTime() >= expirationDate.getTime();
        }
      },
      pinned: 'right'
    }
  ];

  constructor(
    private readonly medicineInventoryHttpService: MedicineInventoryHttpService,
    private readonly toolbarService: ToolbarService,
    private readonly matDialog: MatDialog,
    private readonly matSnackBarViewService: MatSnackBarViewService,
  ) {
  }

  private delete(data: MedicineInventoryModel): void {
    const message = `Confirma que quiere eliminar el inventario de ${data.medicineName}, No. de lote ${data.batchNumber}?`;
    const dialogRef = this.matDialog
      .open<ConfirmationDialogComponent, ConfirmationDialogData, boolean>(ConfirmationDialogComponent, {
        width: '400px',
        data: {
          title: 'Eliminar inventario',
          question: message
        }
      });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        data.comments = 'Desactivado por vencimiento de medicamento.';
        this.toolbarService.showProgressBar(true);

        this.medicineInventoryHttpService
          .deactivate(data)
          .then(() => {
            this.matSnackBarViewService.showSnackBar('Inventario desactivado exitosamente');
            this.loadInventory();
          })
          .finally(() => this.toolbarService.showProgressBar(false));
      }
    });
  }

  private edit(data: MedicineInventoryModel): void {
    const dialogRef = this.matDialog
      .open<MedicineInventoryDialogComponent, GenericDialogData, MedicineInventoryModel>(MedicineInventoryDialogComponent, {
        width: '500px',
        height: '85%',
        panelClass: 'mat-dialog-without-padding',
        data: {
          operationType: 'EDIT',
          payload: data
        }
      });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.matSnackBarViewService.showSnackBar('Inventario actualizado exitosamente!');
      }
    });
  }

  public onAdd(): void {
    const dialogRef = this.matDialog
      .open<MedicineInventoryDialogComponent, GenericDialogData, MedicineInventoryModel>(MedicineInventoryDialogComponent, {
        width: '500px',
        height: '85%',
        panelClass: 'mat-dialog-without-padding',
        data: {
          operationType: 'ADD',
        }
      });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.matSnackBarViewService.showSnackBar('Inventario creado exitosamente!');
        this.inventoryList.push(result);
        this.inventoryList = [...this.inventoryList];
      }
    });
  }

  private loadInventory(): void {
    this.toolbarService.setShowSearchBar(true);

    this.medicineInventoryHttpService
      .findAll()
      .then(response => {
        this.inventoryList = [...response];
        this.allInventoryList = [...response];
      })
      .catch(err => console.error(err))
      .finally(() => this.toolbarService.setShowSearchBar(false));
  }

  searchMedicine(filterValue: string): void {
    if (filterValue) {
      const result = this.allInventoryList.filter(it => searchByLowerCaseText(it.medicineName, filterValue));
      this.inventoryList = [...result];
    } else {
      this.inventoryList = [...this.allInventoryList];
    }
  }

  ngOnInit(): void {
    this.loadInventory();
  }

}
