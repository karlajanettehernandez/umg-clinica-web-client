import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { GenericDialogData } from '../../../../models/common';
import { MedicineInventoryModel } from '../../../../models/inventory';
import { MedicineModel } from '../../../../models/clinic';
import { DatePipe } from '@angular/common';
import { MedicineHttpService } from '../../../../http-services/clinic/medicine.http.service';
import { ReplaySubject } from 'rxjs';
import { searchByLowerCaseText } from '../../../../constants/constants';
import { MedicineInventoryHttpService } from '../../../../http-services/clinic/medicine-inventory.http.service';

@Component({
  selector: 'app-medicine-inventory-dialog',
  templateUrl: './medicine-inventory-dialog.component.html',
  styleUrls: ['./medicine-inventory-dialog.component.scss']
})
export class MedicineInventoryDialogComponent implements OnInit {

  private readonly datePipe = new DatePipe('en-US');

  allMedicine: MedicineModel[] = [];
  filteredMedicine = new ReplaySubject<MedicineModel[]>(1);
  isEditMode = false;
  loading = false;
  dialogTitle = 'Agregar existencias';
  okButton = 'Guardar';
  inventoryForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<GenericDialogData, MedicineInventoryModel>,
    @Inject(MAT_DIALOG_DATA) public data: GenericDialogData,
    private readonly medicineHttpService: MedicineHttpService,
    private readonly medicineInventoryHttpService: MedicineInventoryHttpService
  ) {
    if (data.operationType === 'EDIT') {
      this.isEditMode = true;
      this.dialogTitle = 'Actualizar existencias';
      this.okButton = 'Actualizar';

      const inventoryData = this.data.payload as MedicineInventoryModel;

      this.inventoryForm = this.formBuilder.group({
        id: [inventoryData.id, Validators.required],
        medicineId: [inventoryData.medicineId, Validators.required],
        quantity: [inventoryData.quantity, [Validators.required, Validators.min(1)]],
        purchasePrice: [inventoryData.purchasePrice, Validators.required],
        salePrice: [inventoryData.salePrice, Validators.required],
        batchNumber: [inventoryData.batchNumber],
        expirationDate: [`${inventoryData.expirationDate}T23:59:59-06:00`, Validators.required],
        active: [inventoryData.active],
        quantitySale: [inventoryData.quantitySale],
      });
    } else {
      this.inventoryForm = this.formBuilder.group({
        id: [0, Validators.required],
        medicineId: [null, Validators.required],
        quantity: [0, [Validators.required, Validators.min(1)]],
        purchasePrice: [0, Validators.required],
        salePrice: [0, Validators.required],
        batchNumber: [''],
        expirationDate: [new Date(), Validators.required],
        active: [true]
      });
    }
  }

  private loadMedicineList(): void {
    this.medicineHttpService
      .getList()
      .then(response => {
        this.allMedicine = [
          {
            id: 0,
            name: '-Agregar nuevo medicamento-'
          },
          ...response
        ];
        this.filteredMedicine.next([...this.allMedicine]);
      })
      .catch(err => console.error(err));
  }

  onSelectMedicine(): void {
    if (this.inventoryForm.controls.medicineId.value === 0) {
      this.inventoryForm.addControl('medicineName', new FormControl('', Validators.required))
    } else {
      this.inventoryForm.removeControl('medicineName');
    }
  }

  onFilterMedicineList(filterValue: string): void {
    if (filterValue) {
      const result = this.allMedicine.filter(medicine => searchByLowerCaseText(medicine.name, filterValue));
      this.filteredMedicine.next(result);
    } else {
      this.filteredMedicine.next([...this.allMedicine]);
    }
  }

  cancelar(): void {
    this.dialogRef.close(null);
  }

  aceptar(): void {
    if (this.inventoryForm.invalid) {
      return;
    }

    if (this.data.operationType === 'ADD') {
      this.create();
    } else {
      this.update();
    }

  }

  private create(): void {
    this.loading = true;

    const formData = this.inventoryForm.getRawValue();
    const data: MedicineInventoryModel = {
      ...formData,
      expirationDate: this.datePipe.transform(formData.expirationDate, 'yyyy-MM-dd')
    };

    this.medicineInventoryHttpService
      .create(data)
      .then(response => {
        if (response) {
          data.id = response;
          data.medicineName = this.getMedicineName(data.medicineId);
          data.quantitySale = 0;
          data.stock = data.quantity;
          this.dialogRef.close(data);
        }
      })
      .catch(err => console.error(err))
      .finally(() => this.loading = false);
  }

  private update(): void {
    this.loading = true;

    const formData = this.inventoryForm.getRawValue();
    const data: MedicineInventoryModel = {
      ...formData,
      expirationDate: this.datePipe.transform(formData.expirationDate, 'yyyy-MM-dd')
    };

    this.medicineInventoryHttpService
      .update(data)
      .then(() => {
        data.medicineName = this.getMedicineName(data.medicineId);
        data.stock = data.quantity - data.quantitySale;
        this.dialogRef.close(data);
      })
      .catch(err => console.error(err))
      .finally(() => this.loading = false);
  }

  private getMedicineName(medicineId: number): string {
    if (medicineId === 0) {
      return this.inventoryForm.controls.medicineName.value;
    }

    const data = this.allMedicine.find(it => it.id === medicineId);

    if (data) {
      return data.name;
    } else {
      return '';
    }
  }

  ngOnInit(): void {
    this.loadMedicineList();
  }

}
