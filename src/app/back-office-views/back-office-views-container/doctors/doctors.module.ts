import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DoctorsComponent } from './doctors.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MAT_DATE_LOCALE, MatNativeDateModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CardLayoutModule } from '../../../shared/card-layout/card-layout.module';
import { AgGridModule } from 'ag-grid-angular';
import { AgGridComponentsModule } from '../../../shared/ag-grid-components/ag-grid-components.module';
import { MatDialogModule } from '@angular/material/dialog';
import { ConfirmationDialogModule } from '../../../shared/confirmation-dialog/confirmation-dialog.module';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { DoctorDialogComponent } from './doctor-dialog/doctor-dialog.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ViewsServicesModule } from '../../../views-services/views-services.module';
import { SearchBarModule } from '../../../shared/search-bar/search-bar.module';

const routes: Routes = [
  {
    path: '',
    component: DoctorsComponent
  }
];

@NgModule({
  declarations: [DoctorsComponent, DoctorDialogComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatCheckboxModule,
        MatSnackBarModule,
        MatNativeDateModule,
        MatButtonModule,
        FlexLayoutModule,
        CardLayoutModule,
        AgGridModule,
        AgGridComponentsModule,
        MatDialogModule,
        ConfirmationDialogModule,
        MatSlideToggleModule,
        MatSelectModule,
        MatProgressBarModule,
        MatDatepickerModule,
        MatNativeDateModule,
        ViewsServicesModule,
        SearchBarModule
    ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }
  ]
})
export class DoctorsModule {
}
