import { Component, OnInit } from '@angular/core';
import { DoctorModel } from '../../../models/doctor';
import { DatePipe } from '@angular/common';
import { ColDef, GridOptions } from 'ag-grid-community';
import {
  DATE_DD_MM_YYYY_FORMAT,
  DATE_TIME_DD_MM_YYYY_HH_MM_SS_FORMAT,
  searchByLowerCaseText
} from '../../../constants/constants';
import {
  AgGridIconCheckComponent
} from '../../../shared/ag-grid-components/ag-grid-icon-check/ag-grid-icon-check.component';
import {
  AgGridIconButtonComponent
} from '../../../shared/ag-grid-components/ag-grid-icon-button/ag-grid-icon-button.component';
import { DoctorHttpService } from '../../../http-services/doctor/doctor.http.service';
import { GenericDialogData } from '../../../models/common';
import { DoctorDialogComponent } from './doctor-dialog/doctor-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import {
  ConfirmationDialogComponent,
  ConfirmationDialogData
} from '../../../shared/confirmation-dialog/confirmation-dialog.component';
import { MatSnackBarViewService } from '../../../views-services/mat-snack-bar.view.service';
import { ToolbarService } from '../../../shared/toolbar/toolbar.service';

@Component({
  selector: 'app-doctors',
  templateUrl: './doctors.component.html',
  styleUrls: ['./doctors.component.scss']
})
export class DoctorsComponent implements OnInit {

  private readonly datePipe = new DatePipe('en-US');

  allDoctors: DoctorModel[] = [];
  doctors: DoctorModel[] = [];

  gridOptions: GridOptions = {
    rowHeight: 30
  };

  defaultColDef = {
    resizable: true
  };

  columnDefs: ColDef[] = [
    {
      headerName: 'ID',
      field: 'id',
      width: 50
    },
    {
      headerName: 'Apellidos',
      field: 'lastName',
      width: 150
    },
    {
      headerName: 'Nombres',
      field: 'name',
      width: 150
    },
    {
      headerName: 'No. identificación',
      field: 'identificationDocumentNumber',
      width: 150
    },
    {
      headerName: 'Tipo de identificación',
      field: 'identificationDocumentTypeName',
      width: 275
    },
    {
      headerName: 'No. de colegiado',
      field: 'activeCollegiate',
      width: 150
    },
    {
      headerName: 'Fecha de nacimiento',
      field: 'birthDate',
      width: 150,
      cellRenderer: params => this.datePipe.transform(params.value, DATE_DD_MM_YYYY_FORMAT)
    },
    {
      headerName: 'Especialidad',
      field: 'specialtyName',
      width: 125
    },
    {
      headerName: 'Género',
      field: 'genderName',
      width: 125
    },
    {
      headerName: 'Correo',
      field: 'email',
      width: 250
    },
    {
      headerName: 'Número de teléfono',
      field: 'phoneNumber',
      width: 250
    },
    {
      headerName: 'Dirección',
      field: 'address',
      width: 250
    },
    {
      headerName: 'Activo',
      field: 'active',
      cellRendererFramework: AgGridIconCheckComponent,
      width: 90
    },
    {
      headerName: 'Fecha de creación',
      field: 'createdAt',
      width: 150,
      cellRenderer: params => this.datePipe.transform(params.value, DATE_TIME_DD_MM_YYYY_HH_MM_SS_FORMAT)
    },
    {
      headerName: 'Creado por',
      field: 'createdBy',
      width: 225
    },
    {
      headerName: 'Fecha de modificación',
      field: 'modifiedAt',
      width: 150,
      cellRenderer: params => this.datePipe.transform(params.value, DATE_TIME_DD_MM_YYYY_HH_MM_SS_FORMAT)
    },
    {
      headerName: 'Modificado por',
      field: 'modifiedBy',
      width: 225
    },
    {
      headerName: 'Editar',
      field: 'edit',
      width: 90,
      cellRendererFramework: AgGridIconButtonComponent,
      cellRendererParams: {
        iconName: 'edit',
        buttonTitle: 'Editar',
        onAction: (data: DoctorModel) => this.edit(data)
      },
      pinned: 'right'
    },
    {
      headerName: 'Desactivar',
      field: 'delete',
      width: 125,
      cellRendererFramework: AgGridIconButtonComponent,
      cellRendererParams: {
        iconName: 'cancel',
        buttonTitle: 'Eliminar paciente',
        onAction: (data: DoctorModel) => this.delete(data),
        onShowButton: (data: DoctorModel) => data.active
      },
      pinned: 'right'
    }
  ];

  constructor(
    private readonly doctorHttpService: DoctorHttpService,
    private readonly dialog: MatDialog,
    private readonly matSnackBarViewService: MatSnackBarViewService,
    private readonly toolbarService: ToolbarService
  ) {
    this.toolbarService.setShowSearchBar(false);
  }

  private edit(data: DoctorModel): void {
    const dialogRef = this.dialog
      .open<DoctorDialogComponent, GenericDialogData, boolean>(DoctorDialogComponent, {
        width: '500px',
        height: '85%',
        panelClass: 'mat-dialog-without-padding',
        data: {
          operationType: 'EDIT',
          payload: data
        }
      });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.matSnackBarViewService.showSnackBar('Médico actualizado exitosamente!');
        this.loadDoctors();
      }
    });
  }

  private delete(data: DoctorModel): void {
    const dialogRef = this.dialog
      .open<ConfirmationDialogComponent, ConfirmationDialogData, boolean>(ConfirmationDialogComponent, {
        width: '400px',
        data: {
          title: 'Eliminar médico',
          question: `Confirma que quiere eliminar el médico ${data.id} - ${data.name}?`
        }
      });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.doctorHttpService
          .delete(data.id)
          .then(() => {
            this.matSnackBarViewService.showSnackBar('Médico eliminado exitosamente!', 'OK');
            this.loadDoctors();
          })
          .catch(err => {
            console.error(err);
            this.matSnackBarViewService.showSnackBar('No es posible realizar esta acción, intente mas tarde', 'OK', false);
          });
      }
    });
  }

  private loadDoctors(): void {
    this.toolbarService.showProgressBar(true);
    this.doctorHttpService
      .getAll()
      .then(data => {
        this.doctors = [...data];
        this.allDoctors = [...data];
      })
      .catch(err => console.error(err))
      .finally(() => this.toolbarService.showProgressBar(false));
  }

  onAdd(): void {
    const dialogRef = this.dialog
      .open<DoctorDialogComponent, GenericDialogData, boolean>(DoctorDialogComponent, {
        width: '500px',
        height: '85%',
        panelClass: 'mat-dialog-without-padding',
        data: {
          operationType: 'ADD',
        }
      });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.matSnackBarViewService.showSnackBar('Médico creado exitosamente!');
        this.loadDoctors();
      }
    });
  }

  searchDoctor(filterValue: string): void {
    if (filterValue) {
      const result = this.allDoctors.filter(doctor => this.searchByDoctorNames(doctor, filterValue));
      this.doctors = [...result];
    } else {
      this.doctors = [...this.allDoctors];
    }
  }

  private searchByDoctorNames(doctor: DoctorModel, filterValue: string): boolean {
    return searchByLowerCaseText(doctor.name, filterValue) ||
      searchByLowerCaseText(doctor.lastName, filterValue) ||
      searchByLowerCaseText(doctor.identificationDocumentNumber, filterValue);
  }

  ngOnInit(): void {
    this.loadDoctors();
  }

}
