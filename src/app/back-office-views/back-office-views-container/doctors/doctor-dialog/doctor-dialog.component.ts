import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DocumentTypeModel, GenericDialogData, PersonGenderModel, SpecialtyModel } from '../../../../models/common';
import { DoctorModel } from '../../../../models/doctor';
import { PersonGenderHttpService } from '../../../../http-services/person/person-gender.http.service';
import { DocumentTypeHttpService } from '../../../../http-services/person/document-type.http.service';
import { MatSnackBarViewService } from '../../../../views-services/mat-snack-bar.view.service';
import { DoctorSpecialtyHttpService } from '../../../../http-services/doctor/doctor-specialty.http.service';
import { HttpErrorResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { DoctorHttpService } from '../../../../http-services/doctor/doctor.http.service';
import { DPI_PATTERN, PHONE_NUMBER_PATTERN } from '../../../../constants/constants';

@Component({
  selector: 'app-doctor-dialog',
  templateUrl: './doctor-dialog.component.html',
  styleUrls: ['./doctor-dialog.component.scss']
})
export class DoctorDialogComponent implements OnInit {

  private readonly datePipe = new DatePipe('en-US');

  loading = false;

  isEditMode = false;

  doctorForm: FormGroup;
  dialogTitle = 'Agregar médico';
  okButton = 'Agregar';
  genderList: PersonGenderModel[] = [];
  specialtyList: SpecialtyModel[] = [];
  documentTypeList: DocumentTypeModel[] = [];

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<GenericDialogData, boolean>,
    @Inject(MAT_DIALOG_DATA) public data: GenericDialogData,
    private readonly personGenderHttpService: PersonGenderHttpService,
    private readonly documentTypeHttpService: DocumentTypeHttpService,
    private readonly doctorSpecialtyHttpService: DoctorSpecialtyHttpService,
    private readonly matSnackBarViewService: MatSnackBarViewService,
    private readonly doctorHttpService: DoctorHttpService,
  ) {
    if (data.operationType === 'EDIT') {
      this.isEditMode = true;
      this.dialogTitle = 'Editar doctor';
      this.okButton = 'Guardar';

      const doctor = data.payload as DoctorModel;

      this.doctorForm = this.formBuilder.group({
        id: [doctor.id, Validators.required],
        name: [doctor.name, Validators.required],
        lastName: [doctor.lastName, Validators.required],
        email: [doctor.email, Validators.required],
        birthDate: [new Date(`${doctor.birthDate}T00:00:00`), Validators.required],
        genderId: [doctor.genderId, Validators.required],
        phoneNumber: [doctor.phoneNumber, [Validators.required, Validators.pattern(PHONE_NUMBER_PATTERN)]],
        address: [doctor.address, Validators.required],
        activeCollegiate: [doctor.activeCollegiate, Validators.required],
        identificationDocumentNumber: [
          doctor.identificationDocumentNumber,
          [Validators.required, Validators.pattern(DPI_PATTERN)]
        ],
        identificationDocumentTypeId: [0],
        specialtyId: [doctor.specialtyId, Validators.required],
        active: [doctor.active]
      });

    } else {
      this.doctorForm = this.formBuilder.group({
        id: [0],
        name: ['', Validators.required],
        lastName: ['', Validators.required],
        email: ['', Validators.required],
        birthDate: [new Date(), Validators.required],
        genderId: [null, Validators.required],
        phoneNumber: ['', [Validators.required, Validators.pattern(PHONE_NUMBER_PATTERN)]],
        address: ['', Validators.required],
        activeCollegiate: ['', Validators.required],
        identificationDocumentNumber: [
          '',
          [Validators.required, Validators.pattern(DPI_PATTERN)]
        ],
        identificationDocumentTypeId: [0],
        specialtyId: [null, Validators.required],
        active: [true]
      });
    }
  }

  cancelar(): void {
    this.dialogRef.close(false);
  }

  aceptar(): void {
    if (this.doctorForm.invalid) {
      return;
    }

    this.loading = true;

    const data: DoctorModel = {
      ...this.doctorForm.getRawValue(),
      birthDate: this.datePipe.transform(this.doctorForm.controls.birthDate.value, 'yyyy-MM-dd')
    };

    if (this.isEditMode) {
      this.doctorHttpService
        .update(data)
        .then(() => this.dialogRef.close(true))
        .catch(err => this.processError(err))
        .finally(() => this.loading = false);
    } else {
      this.doctorHttpService
        .create(data)
        .then(response => {
          if (response > 0) {
            this.dialogRef.close(true);
          }
        })
        .catch(err => this.processError(err))
        .finally(() => this.loading = false);
    }
  }

  private processError(err: any): void {
    console.error(err);

    const errorResponse = err as HttpErrorResponse;

    if (errorResponse.error.mensaje) {
      this.matSnackBarViewService.showSnackBar(errorResponse.error.mensaje, 'OK', false);
    }
  }

  private loadPersonGenders(): void {
    this.personGenderHttpService
      .findAll()
      .then(response => this.genderList = response)
      .catch(err => console.error(err));
  }

  // private loadDocumentTypes(): void {
  //   this.documentTypeHttpService
  //     .findAll()
  //     .then(response => this.documentTypeList = response)
  //     .catch(err => console.error(err));
  // }

  private loadSpecialty(): void {
    this.doctorSpecialtyHttpService
      .findAll()
      .then(response => this.specialtyList = response)
      .catch(err => console.log(err));
  }

  ngOnInit(): void {
    this.loadPersonGenders();
    // this.loadDocumentTypes();
    this.loadSpecialty();
  }

}
