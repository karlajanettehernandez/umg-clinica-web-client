import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackOfficeViewsContainerComponent } from './back-office-views-container.component';
import { RouterModule, Routes } from '@angular/router';
import { PrivateContentGuard } from '../../guards/private-content.guard';
import { MainLayoutModule } from '../../shared/main-layout/main-layout.module';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { ToolbarModule } from '../../shared/toolbar/toolbar.module';
import { FlexLayoutModule } from '@angular/flex-layout';

const routes: Routes = [
  {
    path: '',
    component: BackOfficeViewsContainerComponent,
    canActivate: [PrivateContentGuard],
    children: [
      {
        path: 'users',
        loadChildren: () => import('./users/users.module').then(m => m.UsersModule)
      },
      {
        path: 'schedule',
        loadChildren: () => import('./schedule/schedule.module').then(m => m.ScheduleModule)
      },
      {
        path: 'patients',
        loadChildren: () => import('./patients/patients.module').then(m => m.PatientsModule)
      },
      {
        path: 'doctors',
        loadChildren: () => import('./doctors/doctors.module').then(m => m.DoctorsModule)
      },
      {
        path: 'roles',
        loadChildren: () => import('./roles/roles.module').then(m => m.RolesModule)
      },
      {
        path: 'inventory',
        loadChildren: () => import('./medicine-inventory/medicine-inventory.module').then(m => m.MedicineInventoryModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      },
    ]
  }
];

@NgModule({
  declarations: [BackOfficeViewsContainerComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MainLayoutModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    ToolbarModule,
    FlexLayoutModule
  ]
})
export class BackOfficeViewsContainerModule {
}
