import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CreateMedicalHistoryComponent } from '../create-medical-history/create-medical-history.component';
import { Subject } from 'rxjs';
import { ToolbarService } from '../../../../shared/toolbar/toolbar.service';
import { ColDef, GridOptions } from 'ag-grid-community';
import { DATE_TIME_DD_MM_YYYY_HH_MM_SS_FORMAT, searchByLowerCaseText } from '../../../../constants/constants';
import {
  AgGridIconButtonComponent
} from '../../../../shared/ag-grid-components/ag-grid-icon-button/ag-grid-icon-button.component';
import { PatientModel } from '../../../../models/patients';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { PatientDialogComponent } from '../patient-dialog/patient-dialog.component';
import { GenericDialogData } from '../../../../models/common';
import { MatSnackBarViewService } from '../../../../views-services/mat-snack-bar.view.service';
import { PatientHttpService } from '../../../../http-services/patient/patient.http.service';
import { MedicalHistoryModel } from '../../../../models/medical-history';
import { MedicalHistoryHttpService } from '../../../../http-services/medical-history/medical-history.http.service';
import { ReportsHttpService } from '../../../../http-services/clinic/reports.http.service';
import { PdfPayload, PdfViewerDialogComponent } from '../../../../shared/pdf-viewer-dialog/pdf-viewer-dialog.component';

@Component({
  selector: 'app-patient-medical-history',
  templateUrl: './patient-medical-history.component.html',
  styleUrls: ['./patient-medical-history.component.scss']
})
export class PatientMedicalHistoryComponent implements OnInit, OnDestroy {

  private readonly datePipe = new DatePipe('en-US');
  private readonly unsubscribeAll: Subject<boolean> = new Subject();

  private patientId = 0;
  private schedule = 0;
  private doctorId = 0;

  canAddMedicalHistory = false;
  patientName = 'Loading...';
  patientIdentificationNumber = '0';

  allHistoryList: MedicalHistoryModel[] = [];
  historyList: MedicalHistoryModel[] = [];

  gridOptions: GridOptions = {
    rowHeight: 30
  };

  defaultColDef = {
    resizable: true
  };

  columnDefs: ColDef[] = [
    {
      headerName: 'Fecha',
      field: 'createdAt',
      width: 150,
      cellRenderer: params => this.datePipe.transform(params.value, DATE_TIME_DD_MM_YYYY_HH_MM_SS_FORMAT)
    },
    {
      headerName: 'Médico',
      field: 'doctorName',
      width: 350
    },
    {
      headerName: 'Descripción',
      field: 'description',
      width: 350
    },
    {
      headerName: 'Diagnóstico',
      field: 'diagnostic',
      width: 300
    },
    {
      headerName: 'Fecha de creación',
      field: 'createdAt',
      width: 150,
      cellRenderer: params => this.datePipe.transform(params.value, DATE_TIME_DD_MM_YYYY_HH_MM_SS_FORMAT)
    },
    {
      headerName: 'Creado por',
      field: 'createdBy',
      width: 225
    },
    {
      headerName: 'Fecha de modificación',
      field: 'modifiedAt',
      width: 160,
      cellRenderer: params => this.datePipe.transform(params.value, DATE_TIME_DD_MM_YYYY_HH_MM_SS_FORMAT)
    },
    {
      headerName: 'Modificado por',
      field: 'modifiedBy',
      width: 225
    },
    {
      headerName: 'Ver',
      field: 'edit',
      width: 90,
      cellRendererFramework: AgGridIconButtonComponent,
      cellRendererParams: {
        iconName: 'visibility',
        buttonTitle: 'Ver',
        onAction: (data: PatientModel) => this.view(data)
      },
      pinned: 'right'
    },
    // {
    //   headerName: 'Editar',
    //   field: 'edit',
    //   width: 90,
    //   cellRendererFramework: AgGridIconButtonComponent,
    //   cellRendererParams: {
    //     iconName: 'edit',
    //     buttonTitle: 'Ver',
    //     onAction: (data: PatientModel) => this.edit(data)
    //   },
    //   pinned: 'right'
    // },
    {
      headerName: 'Generar PDF',
      field: 'pdf',
      width: 110,
      cellRendererFramework: AgGridIconButtonComponent,
      cellRendererParams: {
        iconName: 'picture_as_pdf',
        buttonTitle: 'Generar PDF',
        onAction: (data: PatientModel) => this.viewPdf(data)
      },
      pinned: 'right'
    }
  ];

  constructor(
    private readonly dialog: MatDialog,
    private readonly toolbarService: ToolbarService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly location: Location,
    private readonly router: Router,
    private readonly matSnackBarViewService: MatSnackBarViewService,
    private readonly patientHttpService: PatientHttpService,
    private readonly medicalHistoryHttpService: MedicalHistoryHttpService,
    private readonly reportsHttpService: ReportsHttpService
  ) {
    this.toolbarService.setShowSearchBar(false);
  }

  private viewPdf(data: any): void {
    this.toolbarService.showProgressBar(true);

    this.reportsHttpService
      .getMedicalHistoryReport(data.id)
      .then(blob => {
        const patientNameParts = this.patientName.split(' ');
        const time = new Date().getTime();
        const reportName = patientNameParts.join('_');

        let url = window.URL.createObjectURL(blob);

        // window.open(url, '_blank');

        // let a = document.createElement('a');
        // document.body.appendChild(a);
        // a.setAttribute('style', 'display: none');
        // a.setAttribute('target', 'blank');
        // a.href = url;
        // a.download = `${reportName}_${time}.pdf`;
        // a.click();
        // window.URL.revokeObjectURL(url);
        // a.remove();

        this.showPdfDialog(url, `${reportName}_${time}.pdf`);
      })
      .catch(err => console.error(err))
      .finally(() => this.toolbarService.showProgressBar(false));
  }

  private showPdfDialog(url: string, fileName: string): void {
    this.dialog.open<PdfViewerDialogComponent, PdfPayload, void>(PdfViewerDialogComponent, {
        data: {
          pdfUrl: url,
          fileName
        }
      },
    );
  }

  private view(data: any): void {
    this.dialog.open<CreateMedicalHistoryComponent, GenericDialogData, MedicalHistoryModel>(
      CreateMedicalHistoryComponent, {
        panelClass: 'mat-dialog-without-padding',
        width: '70%',
        height: '90%',
        maxWidth: '100%',
        maxHeight: '100%',
        data: {
          operationType: 'READONLY',
          payload: data
        }
      },
    );
  }

  private edit(data: any): void {
    const dialogRef = this.dialog.open<CreateMedicalHistoryComponent, GenericDialogData, MedicalHistoryModel>(
      CreateMedicalHistoryComponent, {
        panelClass: 'mat-dialog-without-padding',
        width: '70%',
        height: '90%',
        maxWidth: '100%',
        maxHeight: '100%',
        data: {
          operationType: 'EDIT',
          payload: data
        }
      },
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.matSnackBarViewService.showSnackBar('Historia médica actualizada exitosamente');
        // this.historyList.push(result);
        // this.historyList = [...this.historyList];
        this.loadMedicalHistory(this.patientId);
      }
    });
  }

  onBack(): void {
    this.router.navigate(['back-office', 'patients']);
  }

  onAdd(): void {
    const dialogRef = this.dialog.open<CreateMedicalHistoryComponent, GenericDialogData, MedicalHistoryModel>(
      CreateMedicalHistoryComponent, {
        panelClass: 'mat-dialog-without-padding',
        width: '70%',
        height: '90%',
        maxWidth: '100%',
        maxHeight: '100%',
        disableClose: true,
        data: {
          operationType: 'ADD',
          payload: {
            doctorId: this.doctorId,
            patientId: this.patientId
          }
        }
      },
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.matSnackBarViewService.showSnackBar('Historia médica agregada exitosamente');
        // this.historyList.unshift(result);
        // this.historyList = [...this.historyList];
        this.loadMedicalHistory(this.patientId);
      }
    });
  }

  private validatePatientId(patientId: number, schedule: number): void {
    if (patientId > 0) {
      this.loadPatient(patientId);
      this.loadMedicalHistory(patientId);
    } else {
      this.toolbarService.showProgressBar(false);
      this.patientName = this.activatedRoute.snapshot.queryParams.patientName;
      this.patientIdentificationNumber = '0';

      const dialogRef = this.dialog
        .open<PatientDialogComponent, GenericDialogData, number>(PatientDialogComponent, {
          width: '500px',
          height: '85%',
          panelClass: 'mat-dialog-without-padding',
          disableClose: true,
          data: {
            operationType: 'ADD',
            payload: {
              schedule: schedule,
              patientName: this.patientName
            }
          }
        });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.matSnackBarViewService.showSnackBar('Paciente creado exitosamente!');
          this.loadPatient(result);
        } else {
          this.router.navigate(['back-office/schedule']);
        }
      });
    }
  }

  private validatePatientIdAndSchedule(): void {
    const params = this.activatedRoute.snapshot.params;
    const queryParams = this.activatedRoute.snapshot.queryParams;

    if (params.id !== null && params.id !== undefined) {
      this.patientId = parseInt(params.id, 10);
    }

    if (queryParams.schedule !== null && queryParams.schedule !== undefined) {
      this.schedule = parseInt(queryParams.schedule, 10);
    }

    if (queryParams.doctorId !== null && queryParams.doctorId !== undefined) {
      this.doctorId = parseInt(queryParams.doctorId, 10);
    }

    if (this.schedule <= 0 && this.patientId <= 0) {
      this.router.navigate(['back-office/schedule']);
      return;
    }

    if (this.schedule > 0) {
      this.validatePatientId(this.patientId, this.schedule);
    } else {
      this.loadPatient(this.patientId);
      this.loadMedicalHistory(this.patientId);
    }
  }

  private loadPatient(patientId: number): void {
    this.patientId = patientId;
    this.patientHttpService
      .findById(patientId)
      .then(patient => {
        if (patient.id) {
          this.patientName = `${patient.name} ${patient.lastName}`;
          this.patientIdentificationNumber = patient.identificationDocumentNumber;
        }
      })
      .catch(err => console.error(err))
      .finally(() => this.toolbarService.showProgressBar(false));
  }

  private loadMedicalHistory(patientId: number): void {
    this.medicalHistoryHttpService
      .findByPatientId(patientId)
      .then(response => {
        this.historyList = [...response];
        this.allHistoryList = [...response];
      })
      .catch(err => console.error(err));
  }

  search(filterValue: string): void {
    if (filterValue) {
      const result = this.allHistoryList.filter(history => this.searchMedicalHistory(history, filterValue));
      this.historyList = [...result];
    } else {
      this.historyList = [...this.allHistoryList];
    }
  }

  private searchMedicalHistory(history: MedicalHistoryModel, filterValue: string): boolean {
    return searchByLowerCaseText(history.doctorName, filterValue) ||
      searchByLowerCaseText(history.patientName, filterValue) ||
      searchByLowerCaseText(history.description, filterValue);
  }

  private validatePermissions(): void {
    this.canAddMedicalHistory = this.toolbarService.hasPermission('ADD_MEDICAL_HISTORY');

    if (this.toolbarService.hasPermission('EDIT_MEDICAL_HISTORY')) {
      this.columnDefs.push({
        headerName: 'Editar',
        field: 'edit',
        width: 90,
        cellRendererFramework: AgGridIconButtonComponent,
        cellRendererParams: {
          iconName: 'edit',
          buttonTitle: 'Ver',
          onAction: (data: PatientModel) => this.edit(data)
        },
        pinned: 'right'
      });
    }

    if (this.toolbarService.hasPermission('GENERATE_PDF_MEDICAL_HISTORY')) {
      this.columnDefs.push({
        headerName: 'Generar PDF',
        field: 'pdf',
        width: 110,
        cellRendererFramework: AgGridIconButtonComponent,
        cellRendererParams: {
          iconName: 'picture_as_pdf',
          buttonTitle: 'Generar PDF',
          onAction: (data: PatientModel) => this.viewPdf(data)
        },
        pinned: 'right'
      });
    }
  }

  ngOnInit(): void {
    this.toolbarService.showProgressBar(true);
    this.validatePermissions();

    setTimeout(() => {
      this.validatePatientIdAndSchedule();
    }, 1000);
  }

  ngOnDestroy(): void {
    this.unsubscribeAll.next(true);
    this.unsubscribeAll.complete();
  }

}
