import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DocumentTypeModel, GenericDialogData, PersonGenderModel } from '../../../../models/common';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PatientModel } from '../../../../models/patients';
import { PatientHttpService } from '../../../../http-services/patient/patient.http.service';
import { MatSnackBarViewService } from '../../../../views-services/mat-snack-bar.view.service';
import { HttpErrorResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { PersonGenderHttpService } from '../../../../http-services/person/person-gender.http.service';
import { DPI_PATTERN, PHONE_NUMBER_PATTERN } from '../../../../constants/constants';

@Component({
  selector: 'app-patient-dialog',
  templateUrl: './patient-dialog.component.html',
  styleUrls: ['./patient-dialog.component.scss']
})
export class PatientDialogComponent implements OnInit {

  private readonly datePipe = new DatePipe('en-US');

  loading = false;

  isEditMode = false;

  patientForm: FormGroup;
  dialogTitle = 'Agregar paciente';
  okButton = 'Crear';
  genderList: PersonGenderModel[] = [];
  documentTypeList: DocumentTypeModel[] = [];

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<GenericDialogData, number>,
    @Inject(MAT_DIALOG_DATA) public data: GenericDialogData,
    private readonly patientHttpService: PatientHttpService,
    private readonly matSnackBarViewService: MatSnackBarViewService,
    private readonly personGenderHttpService: PersonGenderHttpService,
  ) {
    if (data.operationType === 'EDIT') {
      this.isEditMode = true;
      this.dialogTitle = 'Editar paciente';
      this.okButton = 'Guardar';

      const patient = data.payload as PatientModel;

      this.patientForm = this.formBuilder.group({
        id: [patient.id, Validators.required],
        name: [patient.name, Validators.required],
        lastName: [patient.lastName, Validators.required],
        email: [patient.email, Validators.email],
        birthDate: [new Date(`${patient.birthDate}T00:00:00`), Validators.required],
        genderId: [patient.genderId, Validators.required],
        phoneNumber: [patient.phoneNumber, [Validators.required, Validators.pattern(PHONE_NUMBER_PATTERN)]],
        address: [patient.address, Validators.required],
        identificationDocumentNumber: [
          patient.identificationDocumentNumber,
          [Validators.required, Validators.pattern(DPI_PATTERN)]
        ],
        identificationDocumentTypeId: [0],
        active: [patient.active]
      });

    } else {
      this.patientForm = this.formBuilder.group({
        id: [0, Validators.required],
        name: ['', Validators.required],
        lastName: ['', Validators.required],
        email: ['', Validators.email],
        birthDate: [new Date(), Validators.required],
        genderId: [null, Validators.required],
        phoneNumber: ['', [Validators.required, Validators.pattern(PHONE_NUMBER_PATTERN)]],
        address: ['', Validators.required],
        identificationDocumentNumber: [
          '',
          [Validators.required, Validators.pattern(DPI_PATTERN)]
        ],
        identificationDocumentTypeId: [0],
        active: [true]
      });

      this.validateIfHasPatientNameFromPayload();
    }
  }

  cancelar(): void {
    this.dialogRef.close(null);
  }

  aceptar(): void {
    if (this.patientForm.invalid) {
      return;
    }

    this.loading = true;

    const data: PatientModel = {
      ...this.patientForm.getRawValue(),
      birthDate: this.datePipe.transform(this.patientForm.controls.birthDate.value, 'yyyy-MM-dd')
    };

    if (this.isEditMode) {
      this.patientHttpService
        .update(data)
        .then(() => {
          this.dialogRef.close(data.id);
        })
        .catch(err => this.processError(err))
        .finally(() => {
          this.loading = false;
        });
    } else {
      data.schedule = this.validateIfHasSchedule();

      this.patientHttpService
        .create(data)
        .then(response => {
          if (response > 0) {
            this.dialogRef.close(response);
          }
        })
        .catch(err => this.processError(err))
        .finally(() => {
          this.loading = false;
        });
    }
  }

  private validateIfHasPatientNameFromPayload(): void {
    if (this.data.payload?.patientName) {
      const names = this.data.payload.patientName.split(' ');

      if (names.length > 1) {
        this.patientForm.controls.name.setValue(names[0]);
        this.patientForm.controls.lastName.setValue(names[1]);
      } else {
        this.patientForm.controls.name.setValue(names[0]);
      }
    }
  }

  private validateIfHasSchedule(): number {
    if (this.data.payload.schedule) {
      return this.data.payload.schedule;
    }

    return null;
  }

  private processError(err: any): void {
    const errorResponse = err as HttpErrorResponse;

    if (errorResponse.error.mensaje) {
      this.matSnackBarViewService.showSnackBar(errorResponse.error.mensaje, 'OK', false);
    }
  }

  private loadPersonGenders(): void {
    this.personGenderHttpService
      .findAll()
      .then(response => this.genderList = response)
      .catch(err => console.error(err));
  }

  // private loadDocumentTypes(): void {
  //   this.documentTypeHttpService
  //     .findAll()
  //     .then(response => this.documentTypeList = response)
  //     .catch(err => console.error(err));
  // }

  ngOnInit(): void {
    this.loadPersonGenders();
    // this.loadDocumentTypes();
  }

}
