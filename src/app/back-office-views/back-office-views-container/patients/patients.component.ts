import { Component, OnInit } from '@angular/core';
import { PatientModel } from '../../../models/patients';
import { ColDef, GridOptions } from 'ag-grid-community';
import {
  AgGridIconCheckComponent
} from '../../../shared/ag-grid-components/ag-grid-icon-check/ag-grid-icon-check.component';
import {
  AgGridIconButtonComponent
} from '../../../shared/ag-grid-components/ag-grid-icon-button/ag-grid-icon-button.component';
import { MatDialog } from '@angular/material/dialog';
import { PatientHttpService } from '../../../http-services/patient/patient.http.service';
import { DatePipe } from '@angular/common';
import {
  DATE_DD_MM_YYYY_FORMAT,
  DATE_TIME_DD_MM_YYYY_HH_MM_SS_FORMAT,
  searchByLowerCaseText
} from '../../../constants/constants';
import { GenericDialogData } from '../../../models/common';
import { PatientDialogComponent } from './patient-dialog/patient-dialog.component';
import { MatSnackBarViewService } from '../../../views-services/mat-snack-bar.view.service';
import {
  ConfirmationDialogComponent,
  ConfirmationDialogData
} from '../../../shared/confirmation-dialog/confirmation-dialog.component';
import { Router } from '@angular/router';
import { ToolbarService } from '../../../shared/toolbar/toolbar.service';

@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.scss']
})
export class PatientsComponent implements OnInit {

  private readonly datePipe = new DatePipe('en-US');

  canAddPatient = false;
  allPatients: PatientModel[] = [];
  patients: PatientModel[] = [];

  gridOptions: GridOptions = {
    rowHeight: 30
  };

  defaultColDef = {
    resizable: true
  };

  columnDefs: ColDef[] = [
    {
      headerName: 'ID',
      field: 'id',
      width: 50
    },
    {
      headerName: 'Apellidos',
      field: 'lastName',
      width: 150
    },
    {
      headerName: 'Nombres',
      field: 'name',
      width: 150
    },
    {
      headerName: 'No. identificación',
      field: 'identificationDocumentNumber',
      width: 150
    },
    {
      headerName: 'Tipo de identificación',
      field: 'identificationDocumentTypeName',
      width: 275
    },
    {
      headerName: 'Fecha de nacimiento',
      field: 'birthDate',
      width: 150,
      cellRenderer: params => this.datePipe.transform(params.value, DATE_DD_MM_YYYY_FORMAT)
    },
    {
      headerName: 'Género',
      field: 'genderName',
      width: 125
    },
    {
      headerName: 'Correo',
      field: 'email',
      width: 250
    },
    {
      headerName: 'Número de teléfono',
      field: 'phoneNumber',
      width: 250
    },
    {
      headerName: 'Dirección',
      field: 'address',
      width: 250
    },
    {
      headerName: 'Activo',
      field: 'active',
      cellRendererFramework: AgGridIconCheckComponent,
      width: 90
    },
    {
      headerName: 'Fecha de creación',
      field: 'createdAt',
      width: 150,
      cellRenderer: params => this.datePipe.transform(params.value, DATE_TIME_DD_MM_YYYY_HH_MM_SS_FORMAT)
    },
    {
      headerName: 'Creado por',
      field: 'createdBy',
      width: 225
    },
    {
      headerName: 'Fecha de modificación',
      field: 'modifiedAt',
      width: 150,
      cellRenderer: params => this.datePipe.transform(params.value, DATE_TIME_DD_MM_YYYY_HH_MM_SS_FORMAT)
    },
    {
      headerName: 'Modificado por',
      field: 'modifiedBy',
      width: 225
    }
  ];

  constructor(
    private readonly dialog: MatDialog,
    private readonly patientHttpService: PatientHttpService,
    private readonly matSnackBarViewService: MatSnackBarViewService,
    private readonly router: Router,
    private readonly toolbarService: ToolbarService
  ) {
    this.toolbarService.setPageTitle('Lista de pacientes');
    this.toolbarService.setShowSearchBar(false);
  }

  private deletePatient(data: PatientModel): void {
    const dialogRef = this.dialog
      .open<ConfirmationDialogComponent, ConfirmationDialogData, boolean>(ConfirmationDialogComponent, {
        width: '400px',
        data: {
          title: 'Eliminar paciente',
          question: `Confirma que quiere eliminar el paciente ${data.id} - ${data.name}?`
        }
      });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.patientHttpService
          .delete(data.id)
          .then(() => {
            this.matSnackBarViewService.showSnackBar('Paciente eliminado exitosamente!', 'OK');
            this.loadPatients();
          })
          .catch(err => {
            console.error(err);
            this.matSnackBarViewService.showSnackBar('No es posible realizar esta acción, intente mas tarde', 'OK', false);
          });
      }
    });
  }

  private goToMedicalHistory(data: PatientModel): void {
    this.router.navigate(['back-office', 'patients', `${data.id}`, 'medical-history']);
  }

  private edit(data: PatientModel): void {
    const dialogRef = this.dialog
      .open<PatientDialogComponent, GenericDialogData, boolean>(PatientDialogComponent, {
        width: '500px',
        height: '85%',
        panelClass: 'mat-dialog-without-padding',
        data: {
          operationType: 'EDIT',
          payload: data
        }
      });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.matSnackBarViewService.showSnackBar('Paciente actualizado exitosamente!');
        this.loadPatients();
      }
    });
  }

  private loadPatients(): void {
    this.toolbarService.showProgressBar(true);
    this.patientHttpService
      .getAll()
      .then(data => {
        this.patients = [...data];
        this.allPatients = [...data];
      })
      .catch(err => console.error(err))
      .finally(() => this.toolbarService.showProgressBar(false));
  }

  onAdd(): void {
    const dialogRef = this.dialog
      .open<PatientDialogComponent, GenericDialogData, number>(PatientDialogComponent, {
        width: '500px',
        height: '85%',
        panelClass: 'mat-dialog-without-padding',
        data: {
          operationType: 'ADD',
        }
      });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.matSnackBarViewService.showSnackBar('Paciente creado exitosamente!');
        this.loadPatients();
      }
    });
  }

  searchPatient(filterValue: string): void {
    if (filterValue) {
      const result = this.allPatients.filter(patient => this.searchByPatientNames(patient, filterValue));
      this.patients = [...result];
    } else {
      this.patients = [...this.allPatients];
    }
  }

  private searchByPatientNames(patient: PatientModel, filterValue: string): boolean {
    return searchByLowerCaseText(patient.name, filterValue) ||
      searchByLowerCaseText(patient.lastName, filterValue) ||
      searchByLowerCaseText(patient.identificationDocumentNumber, filterValue);
  }

  private validatePermissions(): void {
    this.canAddPatient = this.toolbarService.hasPermission('ADD_PATIENT');

    if (this.toolbarService.hasPermission('VIEW_MEDICAL_HISTORY')) {
      this.columnDefs.push({
        headerName: 'Historial médico',
        field: 'medicalHistory',
        width: 125,
        cellRendererFramework: AgGridIconButtonComponent,
        cellRendererParams: {
          iconName: 'folder',
          buttonTitle: 'Historial médico',
          onAction: (data: PatientModel) => this.goToMedicalHistory(data)
        },
        pinned: 'right'
      });
    }

    if (this.toolbarService.hasPermission('EDIT_PATIENT')) {
      this.columnDefs.push({
        headerName: 'Editar',
        field: 'edit',
        width: 90,
        cellRendererFramework: AgGridIconButtonComponent,
        cellRendererParams: {
          iconName: 'edit',
          buttonTitle: 'Editar',
          onAction: (data: PatientModel) => this.edit(data)
        },
        pinned: 'right'
      });

      this.columnDefs.push({
        headerName: 'Desactivar',
        field: 'delete',
        width: 125,
        cellRendererFramework: AgGridIconButtonComponent,
        cellRendererParams: {
          iconName: 'cancel',
          buttonTitle: 'Eliminar paciente',
          onAction: (data: PatientModel) => this.deletePatient(data),
          onShowButton: (data: PatientModel) => data.active
        },
        pinned: 'right'
      });
    }

  }

  ngOnInit(): void {
    this.validatePermissions();
    this.loadPatients();
  }

}
