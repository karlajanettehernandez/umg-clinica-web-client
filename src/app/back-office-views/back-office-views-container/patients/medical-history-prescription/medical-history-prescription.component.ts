import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MedicalHistoryPrescriptionModel } from '../../../../models/medical-history';
import { MedicineModel } from '../../../../models/clinic';
import { ReplaySubject } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ColDef, GridOptions } from 'ag-grid-community';
import {
  AgGridIconButtonComponent
} from '../../../../shared/ag-grid-components/ag-grid-icon-button/ag-grid-icon-button.component';
import { searchByLowerCaseText } from '../../../../constants/constants';
import { MedicineHttpService } from '../../../../http-services/clinic/medicine.http.service';
import { MedicalHistoryHttpService } from '../../../../http-services/medical-history/medical-history.http.service';

@Component({
  selector: 'app-medical-history-prescription',
  templateUrl: './medical-history-prescription.component.html',
  styleUrls: ['./medical-history-prescription.component.scss']
})
export class MedicalHistoryPrescriptionComponent implements OnInit {

  @Input()
  medicalHistoryId = 0;

  @Input()
  operationType = 'ADD';

  @Output()
  prescriptionsOutput = new EventEmitter<MedicalHistoryPrescriptionModel[]>();

  medicalHistoryPrescriptionList: MedicalHistoryPrescriptionModel[] = [];
  allMedicine: MedicineModel[] = [];
  filteredMedicine = new ReplaySubject<MedicineModel[]>(1);
  prescriptionForm: FormGroup;

  gridOptions: GridOptions = {
    rowHeight: 30
  };

  defaultColDef = {
    resizable: true
  };

  columnDefs: ColDef[] = [
    {
      headerName: 'Medicamento',
      field: 'medicineName',
      width: 200
    },
    {
      headerName: 'Indicaciones',
      field: 'indications',
      width: 250
    }
  ];

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly medicineHttpService: MedicineHttpService,
    private readonly medicalHistoryHttpService: MedicalHistoryHttpService
  ) {
    this.prescriptionForm = this.formBuilder.group({
      id: [0, Validators.required],
      medicineId: [null, Validators.required],
      indications: ['', Validators.required],
    });
  }

  onFilterMedicineList(filterValue: string): void {
    if (filterValue) {
      const result = this.allMedicine.filter(medicine => searchByLowerCaseText(medicine.name, filterValue));
      this.filteredMedicine.next(result);
    } else {
      this.filteredMedicine.next([...this.allMedicine]);
    }
  }

  delete(data: MedicalHistoryPrescriptionModel): void {
    const result = this.medicalHistoryPrescriptionList.filter(it => it.id !== data.id);
    this.medicalHistoryPrescriptionList = [...result];

    this.sendUpdatedPrescription();
  }

  addPrescription(): void {
    const temporalId = new Date().getTime();
    const formData = this.prescriptionForm.getRawValue();
    const medicine = this.allMedicine.find(it => it.id === formData.medicineId);

    const item: MedicalHistoryPrescriptionModel = {
      ...formData,
      id: temporalId,
      medicineName: medicine?.name ?? ""
    };

    this.medicalHistoryPrescriptionList.push(item);
    this.medicalHistoryPrescriptionList = [...this.medicalHistoryPrescriptionList];

    this.prescriptionForm.get('medicineId').reset();
    this.prescriptionForm.get('indications').reset();

    this.sendUpdatedPrescription();
  }

  private sendUpdatedPrescription(): void {
    if (this.operationType !== 'READONLY') {
      this.prescriptionsOutput.next(this.medicalHistoryPrescriptionList);
    }
  }

  private loadMedicineList(): void {
    this.medicineHttpService
      .getList()
      .then(response => {
        this.allMedicine = [...response];
        this.filteredMedicine.next([...response]);
      })
      .catch(err => console.error(err));
  }

  private addDeleteColumn(): void {
    if (this.operationType !== 'READONLY') {
      this.columnDefs.push({
        headerName: 'Eliminar',
        field: 'delete',
        width: 90,
        cellRendererFramework: AgGridIconButtonComponent,
        cellRendererParams: {
          iconName: 'delete',
          buttonTitle: 'Eliminar',
          onAction: (data: MedicalHistoryPrescriptionModel) => this.delete(data)
        }
      });
    }
  }

  private loadPrescriptionList(): void {
    if (this.operationType !== 'ADD') {
      this.medicalHistoryHttpService
        .findPrescriptionsByMedicalHistoryId(this.medicalHistoryId)
        .then(response => {
          this.medicalHistoryPrescriptionList = [...response];
          this.sendUpdatedPrescription();
        })
        .catch(err => console.error(err))
    }
  }

  ngOnInit(): void {
    this.addDeleteColumn();
    this.loadMedicineList();
    this.loadPrescriptionList();
  }

}
