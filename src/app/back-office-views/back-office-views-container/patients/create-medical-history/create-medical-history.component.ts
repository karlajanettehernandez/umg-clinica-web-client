import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GenericDialogData } from '../../../../models/common';
import { MedicalHistoryHttpService } from '../../../../http-services/medical-history/medical-history.http.service';
import { DoctorHttpService } from '../../../../http-services/doctor/doctor.http.service';
import { DoctorModel } from '../../../../models/doctor';
import { ReplaySubject } from 'rxjs';
import { searchByLowerCaseText } from '../../../../constants/constants';
import {
  MedicalHistoryInventoryReferenceModel,
  MedicalHistoryLabTestModel,
  MedicalHistoryModel,
  MedicalHistoryPrescriptionModel
} from '../../../../models/medical-history';

@Component({
  selector: 'app-create-medical-history',
  templateUrl: './create-medical-history.component.html',
  styleUrls: ['./create-medical-history.component.scss']
})
export class CreateMedicalHistoryComponent implements OnInit {

  medicalHistoryId = 0;
  operationType = 'ADD';
  okButtonDescription = 'Crear historia';
  loading = false;
  medicalHistoryForm: FormGroup;
  allDoctors: DoctorModel[] = [];
  filteredDoctors = new ReplaySubject<DoctorModel[]>(1);

  labTestList: MedicalHistoryLabTestModel[] = [];
  prescriptionList: MedicalHistoryPrescriptionModel[] = [];
  inventoryReferenceList: MedicalHistoryInventoryReferenceModel[] = [];

  constructor(
    private readonly dialogRef: MatDialogRef<GenericDialogData, MedicalHistoryModel>,
    private readonly formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public readonly data: GenericDialogData,
    private readonly medicalHistoryHttpService: MedicalHistoryHttpService,
    private readonly doctorHttpService: DoctorHttpService,
  ) {
    this.operationType = this.data.operationType;

    if (this.data.operationType === 'EDIT' || this.data.operationType === 'READONLY') {
      this.medicalHistoryId = this.data.payload.id;
      this.okButtonDescription = 'Actualizar historia';

      this.medicalHistoryForm = this.formBuilder.group({
        id: [this.data.payload.id, Validators.required],
        doctorId: [this.data.payload.doctorId, Validators.required],
        patientId: [this.data.payload.patientId],
        description: [this.data.payload.description, Validators.required],
        diagnostic: [this.data.payload.diagnostic, Validators.required],
      });

      if (this.data.operationType === 'READONLY') {
        this.medicalHistoryForm.controls.doctorId.disable();
        this.medicalHistoryForm.controls.description.disable();
        this.medicalHistoryForm.controls.diagnostic.disable();
      }
    } else {
      this.medicalHistoryForm = this.formBuilder.group({
        id: [0, Validators.required],
        doctorId: [this.data.payload.doctorId, Validators.required],
        patientId: [this.data.payload.patientId],
        description: ['', Validators.required],
        diagnostic: ['', Validators.required],
      });
    }
  }

  onClose(): void {
    this.dialogRef.close(null);
  }

  ok(): void {
    if (this.data.operationType === 'EDIT') {
      this.edit();
    } else {
      this.create()
    }
  }

  private edit(): void {
    const data: MedicalHistoryModel = this.medicalHistoryForm.getRawValue();
    data.labTestList = this.labTestList;
    data.prescriptionList = this.prescriptionList;
    data.inventoryReferenceList = this.inventoryReferenceList;

    this.loading = true;

    this.medicalHistoryHttpService
      .update(data)
      .then(() => {
        this.dialogRef.close(data);
      })
      .catch(err => console.error(err))
      .finally(() => this.loading = false)
  }

  private create(): void {
    const data: MedicalHistoryModel = this.medicalHistoryForm.getRawValue();
    data.labTestList = this.labTestList;
    data.prescriptionList = this.prescriptionList;
    data.inventoryReferenceList = this.inventoryReferenceList;

    this.loading = true;

    this.medicalHistoryHttpService
      .create(data)
      .then(response => {
        if (response) {
          data.id = response;
          data.createdAt = new Date();

          const doctor = this.allDoctors.find(it => it.id === data.doctorId);

          data.doctorName = doctor?.name ?? "";

          this.dialogRef.close(data);
        }
      })
      .catch(err => console.error(err))
      .finally(() => this.loading = false)

  }

  onFilterDoctors(filterValue: string): void {
    if (filterValue) {
      const result = this.allDoctors.filter(doctor => searchByLowerCaseText(doctor.name, filterValue));
      this.filteredDoctors.next(result);
    } else {
      this.filteredDoctors.next([...this.allDoctors]);
    }
  }

  setLabTestList(list: MedicalHistoryLabTestModel[]): void {
    this.labTestList = list;
  }

  setPrescriptionList(list: MedicalHistoryPrescriptionModel[]): void {
    this.prescriptionList = list;
  }

  setInventoryReferenceList(inventoryReferenceList: MedicalHistoryInventoryReferenceModel[]): void {
    this.inventoryReferenceList = inventoryReferenceList;
  }

  private loadDoctors(): void {
    this.doctorHttpService
      .getList()
      .then(doctors => {
        this.allDoctors = [...doctors];
        this.filteredDoctors.next([...doctors])
      })
      .catch(err => console.error(err));
  }

  ngOnInit(): void {
    this.loadDoctors();
  }

}
