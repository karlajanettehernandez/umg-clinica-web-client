import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientsComponent } from './patients.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MAT_DATE_LOCALE, MatNativeDateModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CardLayoutModule } from '../../../shared/card-layout/card-layout.module';
import { AgGridModule } from 'ag-grid-angular';
import { AgGridComponentsModule } from '../../../shared/ag-grid-components/ag-grid-components.module';
import { MatDialogModule } from '@angular/material/dialog';
import { ConfirmationDialogModule } from '../../../shared/confirmation-dialog/confirmation-dialog.module';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { PatientDialogComponent } from './patient-dialog/patient-dialog.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ViewsServicesModule } from '../../../views-services/views-services.module';
import { PatientMedicalHistoryComponent } from './patient-medical-history/patient-medical-history.component';
import { CreateMedicalHistoryComponent } from './create-medical-history/create-medical-history.component';
import { SearchBarModule } from '../../../shared/search-bar/search-bar.module';
import { MatSimpleTabsModule } from '../../../shared/mat-simple-tabs/mat-simple-tabs.module';
import { MatSelectSearchModule } from '../../../shared/mat-select-search/mat-select-search.module';
import { MedicalHistoryLabTestComponent } from './medical-history-lab-test/medical-history-lab-test.component';
import {
  MedicalHistoryPrescriptionComponent
} from './medical-history-prescription/medical-history-prescription.component';
import { MedicalHistoryPharmacyComponent } from './medical-history-pharmacy/medical-history-pharmacy.component';
import { PdfViewerDialogModule } from '../../../shared/pdf-viewer-dialog/pdf-viewer-dialog.module';

const routes: Routes = [
  {
    path: '',
    component: PatientsComponent
  },
  {
    path: ':id/medical-history',
    component: PatientMedicalHistoryComponent
  }
];

@NgModule({
  declarations: [
    PatientsComponent,
    PatientDialogComponent,
    PatientMedicalHistoryComponent,
    CreateMedicalHistoryComponent,
    MedicalHistoryLabTestComponent,
    MedicalHistoryPrescriptionComponent,
    MedicalHistoryPharmacyComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatNativeDateModule,
    MatButtonModule,
    FlexLayoutModule,
    CardLayoutModule,
    AgGridModule,
    AgGridComponentsModule,
    MatDialogModule,
    ConfirmationDialogModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ViewsServicesModule,
    SearchBarModule,
    MatSimpleTabsModule,
    MatSelectSearchModule,
    PdfViewerDialogModule
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }
  ]
})
export class PatientsModule {
}
