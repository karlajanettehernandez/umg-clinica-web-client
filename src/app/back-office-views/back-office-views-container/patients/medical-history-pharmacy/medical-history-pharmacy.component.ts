import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MedicalHistoryInventoryReferenceModel } from '../../../../models/medical-history';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MedicineInventoryModel } from '../../../../models/inventory';
import { ReplaySubject } from 'rxjs';
import { ColDef, GridOptions } from 'ag-grid-community';
import {
  AgGridIconButtonComponent
} from '../../../../shared/ag-grid-components/ag-grid-icon-button/ag-grid-icon-button.component';
import { MedicineInventoryHttpService } from '../../../../http-services/clinic/medicine-inventory.http.service';
import { searchByLowerCaseText } from '../../../../constants/constants';
import { MedicalHistoryHttpService } from '../../../../http-services/medical-history/medical-history.http.service';
import { MatSnackBarViewService } from '../../../../views-services/mat-snack-bar.view.service';

@Component({
  selector: 'app-medical-history-pharmacy',
  templateUrl: './medical-history-pharmacy.component.html',
  styleUrls: ['./medical-history-pharmacy.component.scss']
})
export class MedicalHistoryPharmacyComponent implements OnInit {

  @Input()
  medicalHistoryId = 0;

  @Input()
  operationType = 'ADD';

  @Output()
  inventoryReferenceListOutput = new EventEmitter<MedicalHistoryInventoryReferenceModel[]>();

  loading = false;
  pharmacyForm: FormGroup;
  allStocks: MedicineInventoryModel[] = [];
  filteredStocks = new ReplaySubject<MedicineInventoryModel[]>(1);
  inventoryReferenceList: MedicalHistoryInventoryReferenceModel[] = [];

  gridOptions: GridOptions = {
    rowHeight: 30
  };

  defaultColDef = {
    resizable: true
  };

  columnDefs: ColDef[] = [
    {
      headerName: 'Medicamento',
      field: 'medicineName',
      width: 200
    },
    {
      headerName: 'Cantidad',
      field: 'quantity',
      width: 200
    }
  ];

  constructor(
    private readonly medicineInventoryHttpService: MedicineInventoryHttpService,
    private readonly formBuilder: FormBuilder,
    private readonly medicalHistoryHttpService: MedicalHistoryHttpService,
    private readonly matSnackBarViewService: MatSnackBarViewService,
  ) {
    this.pharmacyForm = this.formBuilder.group({
      id: [0, Validators.required],
      medecineInventoryId: [null, Validators.required],
      quantity: [0, [Validators.required, Validators.min(1)]],
    });
  }

  onFilterStock(filterValue: string): void {
    if (filterValue) {
      const result = this.allStocks.filter(stock => searchByLowerCaseText(stock.medicineName, filterValue));
      this.filteredStocks.next(result);
    } else {
      this.filteredStocks.next([...this.allStocks]);
    }
  }

  private validateQuantity(quantityToAdd: number, stock: number): void {
    if (quantityToAdd > stock) {
      throw new Error('La cantidad a agregar es mayor que la cantidad en stock');
    }
  }

  private isStockAdded(stockId: number): void {
    const stock = this.inventoryReferenceList.find(it => it.medicineInventoryId === stockId);

    if (stock) {
      throw new Error('El medicamento ya existe en la lista de medicamentos agregados');
    }
  }

  addMedicine(): void {
    try {
      const temporalId = new Date().getTime();
      const formData = this.pharmacyForm.getRawValue();
      const stock = this.allStocks.find(it => it.id === formData.medecineInventoryId);
      const quantityToAdd = parseInt(formData.quantity);

      this.isStockAdded(stock.id);
      this.validateQuantity(quantityToAdd, stock.stock);

      const data: MedicalHistoryInventoryReferenceModel = {
        id: temporalId,
        medicineInventoryId: stock.id,
        medicineName: stock.medicineName,
        quantity: quantityToAdd
      };

      this.inventoryReferenceList.push(data);
      this.inventoryReferenceList = [...this.inventoryReferenceList];

      this.pharmacyForm.get('medecineInventoryId').reset();
      this.pharmacyForm.get('quantity').reset();

      this.sendUpdatedInventoryReference();
    } catch (e) {
      this.matSnackBarViewService.showSnackBar(e.message, 'OK', false);
    }
  }

  delete(data: MedicalHistoryInventoryReferenceModel): void {
    const result = this.inventoryReferenceList.filter(it => it.id !== data.id);
    this.inventoryReferenceList = [...result];

    this.sendUpdatedInventoryReference();
  }

  private sendUpdatedInventoryReference(): void {
    if (this.operationType !== 'READONLY') {
      this.inventoryReferenceListOutput.next(this.inventoryReferenceList);
    }
  }

  private addDeleteColumn(): void {
    if (this.operationType !== 'READONLY') {
      this.columnDefs.push({
        headerName: 'Eliminar',
        field: 'delete',
        width: 90,
        cellRendererFramework: AgGridIconButtonComponent,
        cellRendererParams: {
          iconName: 'delete',
          buttonTitle: 'Eliminar',
          onAction: (data: MedicalHistoryInventoryReferenceModel) => this.delete(data)
        }
      });
    }
  }

  ngOnInit(): void {
    this.addDeleteColumn();

    this.medicineInventoryHttpService
      .findInStock()
      .then(stocks => {
        this.allStocks = [...stocks];
        this.filteredStocks.next([...stocks]);
      })
      .catch(err => console.error(err));

    if (this.operationType !== 'ADD') {
      this.medicalHistoryHttpService
        .findInventoryReferenceByMedicalHistoryId(this.medicalHistoryId)
        .then(response => {
          this.inventoryReferenceList = [...response];
          this.sendUpdatedInventoryReference();
        })
        .catch(err => console.error(err));
    }
  }

}
