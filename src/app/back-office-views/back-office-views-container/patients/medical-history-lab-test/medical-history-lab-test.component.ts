import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { LabTestModel } from '../../../../models/clinic';
import { LabTestHttpService } from '../../../../http-services/clinic/lab-test.http.service';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { searchByLowerCaseText } from '../../../../constants/constants';
import { MedicalHistoryLabTestModel } from '../../../../models/medical-history';
import { ColDef, GridOptions } from 'ag-grid-community';
import {
  AgGridIconButtonComponent
} from '../../../../shared/ag-grid-components/ag-grid-icon-button/ag-grid-icon-button.component';
import { MedicalHistoryHttpService } from '../../../../http-services/medical-history/medical-history.http.service';

@Component({
  selector: 'app-medical-history-lab-test',
  templateUrl: './medical-history-lab-test.component.html',
  styleUrls: ['./medical-history-lab-test.component.scss']
})
export class MedicalHistoryLabTestComponent implements OnInit {

  @Input()
  medicalHistoryId = 0;

  @Input()
  operationType = 'ADD';

  @Output()
  labTestListOutput = new EventEmitter<MedicalHistoryLabTestModel[]>();

  medicalHistoryLabTestList: MedicalHistoryLabTestModel[] = [];
  allLabTest: LabTestModel[] = [];
  filteredLabTest = new ReplaySubject<LabTestModel[]>(1);
  labTestForm: FormGroup;

  gridOptions: GridOptions = {
    rowHeight: 30
  };

  defaultColDef = {
    resizable: true
  };

  columnDefs: ColDef[] = [
    {
      headerName: 'Examen',
      field: 'labTestName',
      width: 200
    },
    {
      headerName: 'Descripción',
      field: 'description',
      width: 250
    }
  ];


  constructor(
    private readonly labTestHttpService: LabTestHttpService,
    private readonly formBuilder: FormBuilder,
    private readonly medicalHistoryHttpService: MedicalHistoryHttpService
  ) {
    this.labTestForm = this.formBuilder.group({
      id: [0, Validators.required],
      labTestId: [null, Validators.required],
      description: ['', Validators.required],
    });
  }

  onFilterLabTest(filterValue: string): void {
    if (filterValue) {
      const result = this.allLabTest.filter(labTest => searchByLowerCaseText(labTest.name, filterValue));
      this.filteredLabTest.next(result);
    } else {
      this.filteredLabTest.next([...this.allLabTest]);
    }
  }

  delete(data: MedicalHistoryLabTestModel): void {
    const result = this.medicalHistoryLabTestList.filter(it => it.id !== data.id);
    this.medicalHistoryLabTestList = [...result];

    this.sendUpdatedLabTestList();
  }

  addLabTest(): void {
    const temporalId = new Date().getTime();
    const formData = this.labTestForm.getRawValue();
    const labTest = this.allLabTest.find(it => it.id === formData.labTestId);

    const item: MedicalHistoryLabTestModel = {
      ...formData,
      id: temporalId,
      labTestName: labTest?.name ?? ""
    };

    this.medicalHistoryLabTestList.push(item);
    this.medicalHistoryLabTestList = [...this.medicalHistoryLabTestList];

    this.labTestForm.get('labTestId').reset();
    this.labTestForm.get('description').reset();

    this.sendUpdatedLabTestList();
  }

  private sendUpdatedLabTestList(): void {
    if (this.operationType !== 'READONLY') {
      this.labTestListOutput.next(this.medicalHistoryLabTestList);
    }
  }

  private loadLabTestList(): void {
    this.labTestHttpService
      .getList()
      .then(response => {
        this.allLabTest = [...response];
        this.filteredLabTest.next([...response]);
      })
      .catch(err => console.error(err));
  }

  private loadMedicalHistoryLabTestList(): void {
    if (this.operationType !== 'ADD') {
      this.medicalHistoryHttpService
        .findLabTestByMedicalHistoryId(this.medicalHistoryId)
        .then(response => {
          this.medicalHistoryLabTestList = [...response];
          this.sendUpdatedLabTestList();
        })
        .catch(err => console.error(err))
    }
  }

  private addDeleteColumn(): void {
    if (this.operationType !== 'READONLY') {
      this.columnDefs.push({
        headerName: 'Eliminar',
        field: 'delete',
        width: 90,
        cellRendererFramework: AgGridIconButtonComponent,
        cellRendererParams: {
          iconName: 'delete',
          buttonTitle: 'Eliminar',
          onAction: (data: MedicalHistoryLabTestModel) => this.delete(data)
        }
      });
    }
  }

  ngOnInit(): void {
    this.addDeleteColumn();
    this.loadLabTestList();
    this.loadMedicalHistoryLabTestList();
  }

}
