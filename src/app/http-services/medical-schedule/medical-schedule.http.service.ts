import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TOKEN_NAME } from '../../constants/constants';
import { environment } from '../../../environments/environment';
import { MedicalScheduleModel } from '../../models/medical-schedule';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class MedicalScheduleHttpService {

  constructor(private readonly httpClient: HttpClient) {
  }

  getAll(): Promise<MedicalScheduleModel[]> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}medical-schedule`;

    return this.httpClient.get<MedicalScheduleModel[]>(url, { headers: theHeaders }).toPromise();
  }

  create(data: MedicalScheduleModel): Promise<number> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}medical-schedule`;

    return this.httpClient.post<number>(url, data, { headers: theHeaders }).toPromise();
  }

  update(data: MedicalScheduleModel): Promise<void> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}medical-schedule`;

    return this.httpClient.put<void>(url, data, { headers: theHeaders }).toPromise();
  }

  delete(id: number): Promise<void> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}medical-schedule/${id}`;

    const response = this.httpClient.delete<void>(url, { headers: theHeaders });

    return lastValueFrom(response);
  }

}
