import { NgModule } from '@angular/core';
import { AuthenticationHttpService } from './user/authentication.http.service';
import { UserHttpService } from './user/user.http.service';
import { RoleHttpService } from './user/role.http.service';
import { PatientHttpService } from './patient/patient.http.service';
import { DoctorHttpService } from './doctor/doctor.http.service';
import { DoctorSpecialtyHttpService } from './doctor/doctor-specialty.http.service';
import { DocumentTypeHttpService } from './person/document-type.http.service';
import { PersonGenderHttpService } from './person/person-gender.http.service';
import { MedicalScheduleHttpService } from './medical-schedule/medical-schedule.http.service';
import { ApplicationMenuHttpService } from './application/application-menu.http.service';
import { MedicalHistoryHttpService } from './medical-history/medical-history.http.service';
import { LabTestHttpService } from './clinic/lab-test.http.service';
import { MedicineHttpService } from './clinic/medicine.http.service';
import { MedicineInventoryHttpService } from './clinic/medicine-inventory.http.service';
import { DashboardHttpService } from './clinic/dashboard.http.service';
import { ReportsHttpService } from './clinic/reports.http.service';

@NgModule({
  declarations: [],
  imports: [],
  providers: [
    AuthenticationHttpService,
    UserHttpService,
    RoleHttpService,
    PatientHttpService,
    DoctorHttpService,
    DoctorSpecialtyHttpService,
    DocumentTypeHttpService,
    PersonGenderHttpService,
    MedicalScheduleHttpService,
    ApplicationMenuHttpService,
    MedicalHistoryHttpService,
    LabTestHttpService,
    MedicineHttpService,
    MedicineInventoryHttpService,
    DashboardHttpService,
    ReportsHttpService
  ]
})
export class HttpServicesModule {
}
