import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PatientModel } from '../../models/patients';
import { TOKEN_NAME } from '../../constants/constants';
import { environment } from '../../../environments/environment';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class PatientHttpService {

  constructor(private readonly httpClient: HttpClient) {
  }

  getAll(): Promise<PatientModel[]> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}patient`;

    return this.httpClient.get<PatientModel[]>(url, { headers: theHeaders }).toPromise();
  }

  findById(patientId: number): Promise<PatientModel> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}patient/${patientId}`;

    const response = this.httpClient.get<PatientModel>(url, { headers: theHeaders });

    return lastValueFrom(response);
  }

  create(data: PatientModel): Promise<number> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}patient`;

    return this.httpClient.post<number>(url, data, { headers: theHeaders }).toPromise();
  }

  update(data: PatientModel): Promise<void> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}patient`;

    return this.httpClient.put<void>(url, data, { headers: theHeaders }).toPromise();
  }

  delete(id: number): Promise<void> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}patient/${id}`;

    return this.httpClient.delete<void>(url, { headers: theHeaders }).toPromise();
  }

}
