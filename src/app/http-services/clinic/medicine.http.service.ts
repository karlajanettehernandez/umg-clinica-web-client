import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MedicineModel } from '../../models/clinic';
import { TOKEN_NAME } from '../../constants/constants';
import { environment } from '../../../environments/environment';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class MedicineHttpService {

  constructor(private readonly httpClient: HttpClient) {
  }

  getList(): Promise<MedicineModel[]> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}medicine/list`;
    const response = this.httpClient.get<MedicineModel[]>(url, { headers: theHeaders });

    return lastValueFrom(response);
  }

}
