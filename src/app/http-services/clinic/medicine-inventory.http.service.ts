import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MedicineInventoryModel } from '../../models/inventory';
import { TOKEN_NAME } from '../../constants/constants';
import { environment } from '../../../environments/environment';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class MedicineInventoryHttpService {

  constructor(private readonly httpClient: HttpClient) {
  }

  public findAll(): Promise<MedicineInventoryModel[]> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}medicine-inventory/all`;
    const response = this.httpClient.get<MedicineInventoryModel[]>(url, { headers: theHeaders });

    return lastValueFrom(response);
  }

  public findInStock(): Promise<MedicineInventoryModel[]> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}medicine-inventory/in-stock`;
    const response = this.httpClient.get<MedicineInventoryModel[]>(url, { headers: theHeaders });

    return lastValueFrom(response);
  }

  create(data: MedicineInventoryModel): Promise<number> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}medicine-inventory`;

    const response = this.httpClient.post<number>(url, data, { headers: theHeaders });

    return lastValueFrom(response);
  }

  update(data: MedicineInventoryModel): Promise<void> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}medicine-inventory`;

    const response = this.httpClient.put<void>(url, data, { headers: theHeaders });

    return lastValueFrom(response);
  }

  deactivate(data: MedicineInventoryModel): Promise<void> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}medicine-inventory/deactivate`;

    const response = this.httpClient.put<void>(url, data, { headers: theHeaders });

    return lastValueFrom(response);
  }

}
