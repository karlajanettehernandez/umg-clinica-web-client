import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LabTestModel } from '../../models/clinic';
import { TOKEN_NAME } from '../../constants/constants';
import { environment } from '../../../environments/environment';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class LabTestHttpService {

  constructor(private readonly httpClient: HttpClient) {
  }

  getList(): Promise<LabTestModel[]> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}lab-test/list`;
    const response = this.httpClient.get<LabTestModel[]>(url, { headers: theHeaders });

    return lastValueFrom(response);
  }

}
