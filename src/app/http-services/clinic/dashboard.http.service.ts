import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TOKEN_NAME } from '../../constants/constants';
import { environment } from '../../../environments/environment';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class DashboardHttpService {

  constructor(private readonly httpClient: HttpClient) {
  }

  patientsAttendedToday(): Promise<number> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}dashboard/patients-attended-today`;
    const response = this.httpClient.get<number>(url, { headers: theHeaders });

    return lastValueFrom(response);
  }

  patientsAttendedInTheMonth(): Promise<number> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}dashboard/patients-attended-in-the-month`;
    const response = this.httpClient.get<number>(url, { headers: theHeaders });

    return lastValueFrom(response);
  }

  patientsAttendedInTheLastMonth(): Promise<number> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}dashboard/patients-attended-in-the-last-month`;
    const response = this.httpClient.get<number>(url, { headers: theHeaders });

    return lastValueFrom(response);
  }

}
