import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  MedicalHistoryInventoryReferenceModel,
  MedicalHistoryLabTestModel,
  MedicalHistoryModel,
  MedicalHistoryPrescriptionModel
} from '../../models/medical-history';
import { TOKEN_NAME } from '../../constants/constants';
import { environment } from '../../../environments/environment';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class MedicalHistoryHttpService {

  constructor(private readonly httpClient: HttpClient) {
  }

  findByPatientId(patientId: number): Promise<MedicalHistoryModel[]> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}patient/${patientId}/medical-history`;
    const response = this.httpClient.get<MedicalHistoryModel[]>(url, { headers: theHeaders });

    return lastValueFrom(response);
  }

  create(data: MedicalHistoryModel): Promise<number> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}medical-history`;

    const response = this.httpClient.post<number>(url, data, { headers: theHeaders });

    return lastValueFrom(response);
  }

  update(data: MedicalHistoryModel): Promise<void> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}medical-history`;

    const response = this.httpClient.put<void>(url, data, { headers: theHeaders });

    return lastValueFrom(response);
  }

  findLabTestByMedicalHistoryId(medicalHistoryId: number): Promise<MedicalHistoryLabTestModel[]> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}medical-history/${medicalHistoryId}/lab-test`;

    const response = this.httpClient.get<MedicalHistoryLabTestModel[]>(url, { headers: theHeaders });

    return lastValueFrom(response);
  }

  findPrescriptionsByMedicalHistoryId(medicalHistoryId: number): Promise<MedicalHistoryPrescriptionModel[]> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}medical-history/${medicalHistoryId}/prescriptions`;

    const response = this.httpClient.get<MedicalHistoryPrescriptionModel[]>(url, { headers: theHeaders });

    return lastValueFrom(response);
  }

  findInventoryReferenceByMedicalHistoryId(medicalHistoryId: number): Promise<MedicalHistoryInventoryReferenceModel[]> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}medical-history/${medicalHistoryId}/inventory-reference`;

    const response = this.httpClient.get<MedicalHistoryInventoryReferenceModel[]>(url, { headers: theHeaders });

    return lastValueFrom(response);
  }

}
