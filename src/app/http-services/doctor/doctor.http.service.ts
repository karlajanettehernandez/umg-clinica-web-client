import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DoctorModel } from '../../models/doctor';
import { TOKEN_NAME } from '../../constants/constants';
import { environment } from '../../../environments/environment';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class DoctorHttpService {

  constructor(private readonly httpClient: HttpClient) {
  }

  getAll(): Promise<DoctorModel[]> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}doctor`;

    return this.httpClient.get<DoctorModel[]>(url, { headers: theHeaders }).toPromise();
  }

  getList(): Promise<DoctorModel[]> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}doctor/list`;
    const response = this.httpClient.get<DoctorModel[]>(url, { headers: theHeaders });

    return lastValueFrom(response);
  }

  create(data: DoctorModel): Promise<number> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}doctor`;

    return this.httpClient.post<number>(url, data, { headers: theHeaders }).toPromise();
  }

  update(data: DoctorModel): Promise<void> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}doctor`;

    return this.httpClient.put<void>(url, data, { headers: theHeaders }).toPromise();
  }

  delete(id: number): Promise<void> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}doctor/${id}`;

    return this.httpClient.delete<void>(url, { headers: theHeaders }).toPromise();
  }

}
