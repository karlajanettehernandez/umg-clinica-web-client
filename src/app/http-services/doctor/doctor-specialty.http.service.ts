import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DoctorSpecialtyModel } from '../../models/doctor';
import { TOKEN_NAME } from '../../constants/constants';
import { environment } from '../../../environments/environment';

@Injectable()
export class DoctorSpecialtyHttpService {

  constructor(private readonly httpClient: HttpClient) {
  }

  findAll(): Promise<DoctorSpecialtyModel[]> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}doctor-specialty`;

    return this.httpClient.get<DoctorSpecialtyModel[]>(url, { headers: theHeaders }).toPromise();
  }

}
