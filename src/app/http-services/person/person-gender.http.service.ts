import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PersonGenderModel } from '../../models/common';
import { TOKEN_NAME } from '../../constants/constants';
import { environment } from '../../../environments/environment';

@Injectable()
export class PersonGenderHttpService {

  constructor(private readonly httpClient: HttpClient) {
  }

  findAll(): Promise<PersonGenderModel[]> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}person-gender`;

    return this.httpClient.get<PersonGenderModel[]>(url, { headers: theHeaders }).toPromise();
  }

}
