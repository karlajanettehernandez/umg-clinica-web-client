import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DocumentTypeModel } from '../../models/common';
import { TOKEN_NAME } from '../../constants/constants';
import { environment } from '../../../environments/environment';

@Injectable()
export class DocumentTypeHttpService {

  constructor(private readonly httpClient: HttpClient) {
  }

  findAll(): Promise<DocumentTypeModel[]> {
    const token = localStorage.getItem(TOKEN_NAME);
    const theHeaders = { authorization: `Bearer ${token}` };

    const url = `${environment.api}document-type`;

    return this.httpClient.get<DocumentTypeModel[]>(url, { headers: theHeaders }).toPromise();
  }

}
