#docker stop clinica-frontend-container
#docker rmi wilvermartinezg/clinica-frontend:dev
#cd projects/umg-clinica-web-client
#git pull
#npm run build
#docker build -t wilvermartinezg/clinica-frontend:dev .
#docker run \
#    --name clinica-frontend-container \
#    --rm \
#    -d \
#    -p 80:80 \
#    -e "TZ=America/Guatemala" \
#    wilvermartinezg/clinica-frontend:dev

npm run build && docker build --platform linux/amd64 -t wilvermartinezg/clinica-frontend:dev .
